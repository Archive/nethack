#!/bin/sh

set -e

if [ "x$NETHACKOPTIONS" = x ];
then
    if [ ! -f ~/.gnomehackrc ] ; then
            echo \
    "warning: no .gnomehackrc file in home dir or NETHACKOPTIONS env var"
        if [ -f /etc/gnomehackrc ] ; then
            echo "copying default /etc/gnomehackrc to your home directory."
            echo "you are encouraged to edit it to your liking."
            cp /etc/gnomehackrc ~/.gnomehackrc
            NETHACKOPTIONS=~/.gnomehackrc
            export NETHACKOPTIONS
        else
            echo "warning: no default /etc/gnomehackrc file found..."
        fi;
    else
        NETHACKOPTIONS=~/.gnomehackrc
        export NETHACKOPTIONS
    fi;
fi;
exec /usr/lib/games/gnomehack/gnomehack.sh $*
