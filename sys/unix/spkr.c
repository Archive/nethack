/*
 * spkr.c -- device driver for console speaker on 80386
 *
 * v1.1 by Eric S. Raymond (esr@snark.thyrsus.com) Feb 1990
 *      modified for 386bsd by Andrew A. Chernov <ache@astral.msk.su>
 */

#ifdef __386BSD__
#include "speaker.h"
#endif
#if !defined(__386BSD__) || (NSPEAKER > 0)

#ifdef __386BSD__
#include "types.h"
#include "param.h"
#include "errno.h"
#include "buf.h"
#include "uio.h"

#define CADDR caddr_t
#define err_ret(x) return(x)
#else /* SYSV */
#include <sys/types.h>
#include <sys/param.h>
#include <sys/dir.h>
#include <sys/signal.h>
#include <sys/errno.h>
#include <sys/ioctl.h>
#include <sys/user.h>
#include <sys/sysmacros.h> 
#include <limits.h>

#define CADDR char *
#define err_ret(x) u.u_error = (x)
#endif

#include "spkr.h"

/**************** MACHINE DEPENDENT PART STARTS HERE *************************
 *
 * This section defines a function tone() which causes a tone of given
 * frequency and duration from the 80x86's console speaker.
 * Another function endtone() is defined to force sound off, and there is
 * also a rest() entry point to do pauses.
 *
 * Audible sound is generated using the Programmable Interval Timer (PIT) and
 * Programmable Peripheral Interface (PPI) attached to the 80x86's speaker. The
 * PPI controls whether sound is passed through at all; the PIT's channel 2 is
 * used to generate clicks (a square wave) of whatever frequency is desired.
 *
 * The non-BSD code requires SVr3.2-compatible inb(), outb(), timeout(),
 * sleep(), and wakeup().
 */

/*
 * PIT and PPI port addresses and control values
 *
 * Most of the magic is hidden in the TIMER_PREP value, which selects PIT
 * channel 2, frequency LSB first, square-wave mode and binary encoding.
 * The encoding is as follows:
 *
 * +----------+----------+---------------+-----+
 * |  1    0  |  1    1  |  0    1    1  |  0  |
 * | SC1  SC0 | RW1  RW0 | M2   M1   M0  | BCD |
 * +----------+----------+---------------+-----+
 *   Counter     Write        Mode 3      Binary
 *  Channel 2  LSB first,  (Square Wave) Encoding 
 *             MSB second
 */
#define PPI		0x61	/* port of Programmable Peripheral Interface */
#define PPI_SPKR	0x03	/* turn these PPI bits on to pass sound */
#define PIT_CTRL	0x43	/* PIT control address */
#define PIT_COUNT	0x42	/* PIT count address */
#define PIT_MODE	0xB6	/* set timer mode for sound generation */

/*
 * Magic numbers for timer control. 
 */
#define TIMER_CLK	1193180L	/* corresponds to 18.2 MHz tick rate */

static int endtone()
/* turn off the speaker, ending current tone */
{
    wakeup((CADDR)endtone);
    outb(PPI, inb(PPI) & ~PPI_SPKR);
}

static void tone(hz, ticks)
/* emit tone of frequency hz for given number of ticks */
unsigned int hz, ticks;
{
    unsigned int divisor = TIMER_CLK / hz;
    int sps;

#ifdef DEBUG
    printf("tone: hz=%d ticks=%d\n", hz, ticks);
#endif /* DEBUG */

    /* set timer to generate clicks at given frequency in Hertz */
#ifdef __386BSD__
    sps = spltty();
#else
    sps = spl5();
#endif
    outb(PIT_CTRL, PIT_MODE);		/* prepare timer */
    outb(PIT_COUNT, (unsigned char) divisor);  /* send lo byte */
    outb(PIT_COUNT, (divisor >> 8));	/* send hi byte */
    splx(sps);

    /* turn the speaker on */
    outb(PPI, inb(PPI) | PPI_SPKR);

    /*
     * Set timeout to endtone function, then give up the timeslice.
     * This is so other processes can execute while the tone is being
     * emitted.
     */
    timeout((CADDR)endtone, (CADDR)NULL, ticks);
    sleep((CADDR)endtone, PZERO - 1);
}

static int endrest()
/* end a rest */
{
    wakeup((CADDR)endrest);
}

static void rest(ticks)
/* rest for given number of ticks */
int	ticks;
{
    /*
     * Set timeout to endrest function, then give up the timeslice.
     * This is so other processes can execute while the rest is being
     * waited out.
     */
#ifdef DEBUG
    printf("rest: %d\n", ticks);
#endif /* DEBUG */
    timeout((CADDR)endrest, (CADDR)NULL, ticks);
    sleep((CADDR)endrest, PZERO - 1);
}

#include "interp.c"	/* playinit() and playstring() */

/******************* UNIX DRIVER HOOKS BEGIN HERE **************************
 *
 * This section implements driver hooks to run playstring() and the tone(),
 * endtone(), and rest() functions defined above.  For non-BSD systems,
 * SVr3.2-compatible copyin() is also required.
 */

static int spkr_active;	/* exclusion flag */
#ifdef __386BSD__
static struct  buf *spkr_inbuf; /* incoming buf */
#endif

int spkropen(dev)
dev_t	dev;
{
#ifdef DEBUG
    printf("spkropen: entering with dev = %x\n", dev);
#endif /* DEBUG */

    if (minor(dev) != 0)
	err_ret(ENXIO);
    else if (spkr_active)
	err_ret(EBUSY);
    else
    {
	playinit();
#ifdef __386BSD__
	spkr_inbuf = geteblk(DEV_BSIZE);
#endif
	spkr_active = 1;
    }
#ifdef __386BSD__
    return(0);
#endif
}

#ifdef __386BSD__
int spkrwrite(dev, uio)
struct uio *uio;
#else
int spkrwrite(dev)
#endif
dev_t	dev;
{
#ifdef __386BSD__
    register unsigned n;
    char *cp;
    int error;
#endif
#ifdef DEBUG
#ifdef __386BSD__
    printf("spkrwrite: entering with dev = %x, count = %d\n",
		dev, uio->uio_resid);
#else
    printf("spkrwrite: entering with dev = %x, u.u_count = %d\n",
		dev, u.u_count);
#endif
#endif /* DEBUG */

    if (minor(dev) != 0)
	err_ret(ENXIO);
    else
    {
#ifdef __386BSD__
	n = MIN(DEV_BSIZE, uio->uio_resid);
	cp = spkr_inbuf->b_un.b_addr;
	error = uiomove(cp, n, uio);
	if (!error)
		playstring(cp, n);
	return(error);
#else
	char	bfr[STD_BLK];

	copyin(u.u_base, bfr, u.u_count);
	playstring(bfr, u.u_count);
	u.u_base += u.u_count;
	u.u_count = 0;
#endif
    }
}

int spkrclose(dev)
dev_t	dev;
{
#ifdef DEBUG
    printf("spkrclose: entering with dev = %x\n", dev);
#endif /* DEBUG */

    if (minor(dev) != 0)
	err_ret(ENXIO);
    else
    {
	endtone();
#ifdef __386BSD__
	brelse(spkr_inbuf);
#endif
	spkr_active = 0;
    }
#ifdef __386BSD__
    return(0);
#endif
}

int spkrioctl(dev, cmd, cmdarg)
dev_t	dev;
int	cmd;
CADDR   cmdarg;
{
#ifdef DEBUG
    printf("spkrioctl: entering with dev = %x, cmd = %x\n", dev, cmd);
#endif /* DEBUG */

    if (minor(dev) != 0)
	err_ret(ENXIO);
    else if (cmd == SPKRTONE)
    {
	tone_t	*tp = (tone_t *)cmdarg;

	if (tp->frequency == 0)
	    rest(tp->duration);
	else
	    tone(tp->frequency, tp->duration);
    }
    else if (cmd == SPKRTUNE)
    {
#ifdef __386BSD__
	tone_t  *tp = (tone_t *)(*(caddr_t *)cmdarg);
	tone_t ttp;
	int error;

	for (; ; tp++) {
	    error = copyin(tp, &ttp, sizeof(tone_t));
	    if (error)
		    return(error);
	    if (ttp.duration == 0)
		    break;
	    if (ttp.frequency == 0)
		rest(ttp.duration);
	    else
		tone(ttp.frequency, ttp.duration);
	}
#else
	tone_t	*tp = (tone_t *)cmdarg;

	for (; tp->duration; tp++)
	    if (tp->frequency == 0)
		rest(tp->duration);
	    else
		tone(tp->frequency, tp->duration);
#endif
    }
    else
	err_ret(EINVAL);
#ifdef __386BSD__
    return(0);
#endif
}

#endif  /* !defined(__386BSD__) || (NSPEAKER > 0) */
/* spkr.c ends here */
