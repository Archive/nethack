/*
 * Nethack tiletext format library -- intended for a plugin for the GIMP
 * but can be used anywhere
 *
 * Much of this code is borrowed from win/share/tiletext.c in the standard 
 * Nethack distribution.
 *
 * This code is covered by the Nethack Public License
 *
 * Hacked by Rockwalrus (Nathan Summers)
 */

#include <string.h>
#include "tilelib.h"
#include "libgimp/gimp.h"
#include "glib.h"


/* Ugh.  DICE doesn't like %[A-Z], so we have to spell it out... */
#define FORMAT_STRING \
"%[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789] = (%d, %d, %d) "

static char charcolors[MAXCOLORMAPSIZE];

void
tiletext_read_colormap(txtfile)
tiletext_file *txtfile;
{
	int i, r, g, b, num_colors;
	char c[2];

	g_return_if_fail (txtfile);
	
	for (i = 0; i < MAXCOLORMAPSIZE; i++)
		txtfile->imap.d[i] = -1;

	num_colors = 0;
	while (fscanf(txtfile->file, FORMAT_STRING, c, &r, &g, &b) == 4) {
		txtfile->imap.d[(int) c[0]] = num_colors;
		txtfile->cmap.d[CM_RED][num_colors] = r;
		txtfile->cmap.d[CM_GREEN][num_colors] = g;
		txtfile->cmap.d[CM_BLUE][num_colors] = b;
		num_colors++;
	}
	txtfile->cmap.n = num_colors;
}

#undef FORMAT_STRING

boolean
tiletext_write_colormap(txtfile)
tiletext_file *txtfile;
{
	int i, num_colors;
	char c;

	g_return_if_fail(txtfile);
	
	num_colors = txtfile->cmap.n;
	if (num_colors > 62) {
		g_warning("cannot write %s: too many colors (%d)\n", txtfile->name, num_colors);
		return FALSE;
	}
	for (i = 0; i < num_colors; i++) {
		if (i < 26) c = 'A' + i;
		else if (i < 52) c = 'a' + i - 26;
		else c = '0' + i - 52;

		charcolors[i] = c;
		Fprintf(txtfile->file, "%c = (%d, %d, %d)\n", c,
						(int)txtfile->cmap.d[CM_RED][i],
						(int)txtfile->cmap.d[CM_GREEN][i],
						(int)txtfile->cmap.d[CM_BLUE][i]);
	}
	return TRUE;
}

boolean
tiletext_read(txtfile, t)
tiletext_file *txtfile;
tile	*t;
{
	int i, j, k;
	const char *p;
	char c[2], tile_name[256];

	g_return_if_fail(txtfile && t);
	
	if (fscanf(txtfile->file, "# tile %d (%[^)])", &i, tile_name) <= 0)
		return FALSE;
	
	t->name=strdup(tile_name);
	
	t->data=g_new(pixel *, txtfile->ysize);
	
	for(i = 0; i<txtfile->ysize; i++)
		t->data[i]=g_new(pixel, txtfile->xsize);

	/* look for non-whitespace at each stage */
	if (fscanf(txtfile->file, "%1s", c) < 0) {
		g_warning("unexpected EOF in file %s\n", txtfile->name);
		return FALSE;
	}
	if (c[0] != '{') {
		g_warning("didn't find expected '{' in file %s\n", txtfile->name);
		return FALSE;
	}
	for (j = 0; j < txtfile->ysize; j++) {
		for (i = 0; i < txtfile->xsize; i++) {
			if (fscanf(txtfile->file, "%1s", c) < 0) {
				g_warning("unexpected EOF in file %s\n", txtfile->name);
				return FALSE;
			}
			k = txtfile->imap.d[(int) c[0]];
			if (k == -1)
				g_warning("color %c not in colormap!\n", c[0]);
			else {
				t->data[j][i].r = txtfile->cmap.d[CM_RED][k];
				t->data[j][i].g = txtfile->cmap.d[CM_GREEN][k];
				t->data[j][i].b = txtfile->cmap.d[CM_BLUE][k];
			}
		}
	}
	if (fscanf(txtfile->file, "%1s ", c) < 0) {
		g_warning("unexpected EOF in file %s\n", txtfile->name);
		return FALSE;
	}
	if (c[0] != '}') {
		g_warning("didn't find expected '}' in file %s\n", txtfile->name);
		return FALSE;
	}
#ifdef _DCC
	/* DICE again... it doesn't seem to eat whitespace after the } like
	 * it should, so we have to do so manually.
	 */
	while ((*c = fgetc(txtfile->file)) != EOF && isspace(*c))
		;
	ungetc(*c, txtfile->file);
#endif
	return TRUE;
}

void
tiletext_write(txtfile, t)
tiletext_file *txtfile;
tile *t;

{
	const char *p;
	int i, j, k;

	
	g_return_if_fail(t && txtfile);

	Fprintf(txtfile->file, "{\n");
	for (j = 0; j < txtfile->ysize; j++) {
		Fprintf(txtfile->file, "  ");
		for (i = 0; i < txtfile->xsize; i++) {
			for (k = 0; k < txtfile->cmap.n; k++) {
				if (txtfile->cmap.d[CM_RED][k] == t->data[j][i].r &&
				    txtfile->cmap.d[CM_GREEN][k] == t->data[j][i].g &&
				    txtfile->cmap.d[CM_BLUE][k] == t->data[j][i].b)
					break;
			}
			if (k >= txtfile->cmap.n)
				g_warning("color not in colormap in file %s!\n", txtfile->name);
			(void) fputc(charcolors[k], txtfile->file);
		}
		Fprintf(txtfile->file, "\n");
	}
	Fprintf(txtfile->file, "}\n");
}

/* initialize main colormap from globally accessed ColorMap */
void
copy_colormap(src, dest)
	colormap *src, *dest;
{
	int i;

	g_return_if_fail(src && dest);
	
	dest->n = src->n;
	for (i = 0; i < src->n; i++) {
		dest->d[CM_RED][i] = src->d[CM_RED][i];
		dest->d[CM_GREEN][i] = src->d[CM_GREEN][i];
		dest->d[CM_BLUE][i] =src->d[CM_BLUE][i];
	}
}

/* merge new colors from ColorMap into MainColorMap */
void
merge_colormap(src, dest)
	colormap *src, *dest;
{
	int i, j;

	g_return_if_fail(src && dest);
	
	for (i = 0; i < src->n; i++) {
		for (j = 0; j < dest->n; j++) {
		    if (dest->d[CM_RED][j] == src->d[CM_RED][i] &&
			dest->d[CM_GREEN][j] == src->d[CM_GREEN][i] &&
			dest->d[CM_BLUE][j] == src->d[CM_BLUE][i])
			    break;
		}
		if (j >= dest->n) {	/* new color */
		    if (dest->n >= MAXCOLORMAPSIZE) {
			g_warning("Too many colors to merge -- excess ignored.\n");
		    }
		    j = dest->n;
		    dest->d[CM_RED][j] = src->d[CM_RED][i];
		    dest->d[CM_GREEN][j] = src->d[CM_GREEN][i];
		    dest->d[CM_BLUE][j] = src->d[CM_BLUE][i];
		    dest->n++;
		}
	}
}

void
tiletext_read_size(txtfile)
	tiletext_file *txtfile;
{
	g_return_if_fail(txtfile);
	
	txtfile->xsize = txtfile->ysize = 16;
}

tiletext_file *
tiletext_open(filename, type)
const char *filename;
const char *type;
{
	const char *p;
	int i;
	tiletext_file *f;

	g_return_val_if_fail(filename && type, NULL);

	f=g_new(tiletext_file, 1);
	
	f->file = fopen(filename, type);
	if (!f->file) {
		g_warning("cannot open text file %s\n", filename);
		return FALSE;
	}

	p = strrchr(filename, '/');
	if (p) p++;
	else p = filename;
	f->name = strdup(p);
	return f;
}


int
tiletext_close(f)
	tiletext_file *f;
{
	int ret;

	g_return_val_if_fail(f, -1);
	ret = fclose(f->file);
	f->file = NULL;
	return ret;
}
