
/*  A Bison parser, made from lev_comp.y
 by  GNU Bison version 1.25
  */

#define YYBISON 1  /* Identify Bison output.  */

#define	CHAR	258
#define	INTEGER	259
#define	BOOLEAN	260
#define	PERCENT	261
#define	MESSAGE_ID	262
#define	MAZE_ID	263
#define	LEVEL_ID	264
#define	LEV_INIT_ID	265
#define	GEOMETRY_ID	266
#define	NOMAP_ID	267
#define	OBJECT_ID	268
#define	COBJECT_ID	269
#define	MONSTER_ID	270
#define	TRAP_ID	271
#define	DOOR_ID	272
#define	DRAWBRIDGE_ID	273
#define	MAZEWALK_ID	274
#define	WALLIFY_ID	275
#define	REGION_ID	276
#define	FILLING	277
#define	RANDOM_OBJECTS_ID	278
#define	RANDOM_MONSTERS_ID	279
#define	RANDOM_PLACES_ID	280
#define	ALTAR_ID	281
#define	LADDER_ID	282
#define	STAIR_ID	283
#define	NON_DIGGABLE_ID	284
#define	NON_PASSWALL_ID	285
#define	ROOM_ID	286
#define	PORTAL_ID	287
#define	TELEPRT_ID	288
#define	BRANCH_ID	289
#define	LEV	290
#define	CHANCE_ID	291
#define	CORRIDOR_ID	292
#define	GOLD_ID	293
#define	ENGRAVING_ID	294
#define	FOUNTAIN_ID	295
#define	POOL_ID	296
#define	SINK_ID	297
#define	NONE	298
#define	RAND_CORRIDOR_ID	299
#define	DOOR_STATE	300
#define	LIGHT_STATE	301
#define	CURSE_TYPE	302
#define	ENGRAVING_TYPE	303
#define	DIRECTION	304
#define	RANDOM_TYPE	305
#define	O_REGISTER	306
#define	M_REGISTER	307
#define	P_REGISTER	308
#define	A_REGISTER	309
#define	ALIGNMENT	310
#define	LEFT_OR_RIGHT	311
#define	CENTER	312
#define	TOP_OR_BOT	313
#define	ALTAR_TYPE	314
#define	UP_OR_DOWN	315
#define	SUBROOM_ID	316
#define	NAME_ID	317
#define	FLAGS_ID	318
#define	FLAG_TYPE	319
#define	MON_ATTITUDE	320
#define	MON_ALERTNESS	321
#define	MON_APPEARANCE	322
#define	CONTAINED	323
#define	STRING	324
#define	MAP_ID	325

#line 1 "lev_comp.y"

/*	SCCS Id: @(#)lev_yacc.c	3.2	96/05/16	*/
/*	Copyright (c) 1989 by Jean-Christophe Collet */
/* NetHack may be freely redistributed.  See license for details. */

/*
 * This file contains the Level Compiler code
 * It may handle special mazes & special room-levels
 */

/* In case we're using bison in AIX.  This definition must be
 * placed before any other C-language construct in the file
 * excluding comments and preprocessor directives (thanks IBM
 * for this wonderful feature...).
 *
 * Note: some cpps barf on this 'undefined control' (#pragma).
 * Addition of the leading space seems to prevent barfage for now,
 * and AIX will still see the directive.
 */
#ifdef _AIX
 #pragma alloca		/* keep leading space! */
#endif

#include "hack.h"
#include "sp_lev.h"

#define MAX_REGISTERS	10
#define ERR		(-1)
/* many types of things are put in chars for transference to NetHack.
 * since some systems will use signed chars, limit everybody to the
 * same number for portability.
 */
#define MAX_OF_TYPE	128

#define New(type)		\
	(type *) memset((genericptr_t)alloc(sizeof(type)), 0, sizeof(type))
#define NewTab(type, size)	(type **) alloc(sizeof(type *) * size)
#define Free(ptr)		free((genericptr_t)ptr)

extern void FDECL(yyerror, (const char *));
extern void FDECL(yywarning, (const char *));
extern int NDECL(yylex);
int NDECL(yyparse);

extern int FDECL(get_floor_type, (CHAR_P));
extern int FDECL(get_room_type, (char *));
extern int FDECL(get_trap_type, (char *));
extern int FDECL(get_monster_id, (char *,CHAR_P));
extern int FDECL(get_object_id, (char *));
extern boolean FDECL(check_monster_char, (CHAR_P));
extern boolean FDECL(check_object_char, (CHAR_P));
extern char FDECL(what_map_char, (CHAR_P));
extern void FDECL(scan_map, (char *));
extern void NDECL(wallify_map);
extern boolean NDECL(check_subrooms);
extern void FDECL(check_coord, (int,int,const char *));
extern void NDECL(store_part);
extern void NDECL(store_room);
extern boolean FDECL(write_level_file, (char *,splev *,specialmaze *));
extern void FDECL(free_rooms, (splev *));

static struct reg {
	int x1, y1;
	int x2, y2;
}		current_region;

static struct coord {
	int x;
	int y;
}		current_coord, current_align;

static struct size {
	int height;
	int width;
}		current_size;

char tmpmessage[256];
digpos *tmppass[32];
char *tmpmap[ROWNO];

digpos *tmpdig[MAX_OF_TYPE];
region *tmpreg[MAX_OF_TYPE];
lev_region *tmplreg[MAX_OF_TYPE];
door *tmpdoor[MAX_OF_TYPE];
drawbridge *tmpdb[MAX_OF_TYPE];
walk *tmpwalk[MAX_OF_TYPE];

room_door *tmprdoor[MAX_OF_TYPE];
trap *tmptrap[MAX_OF_TYPE];
monster *tmpmonst[MAX_OF_TYPE];
object *tmpobj[MAX_OF_TYPE];
altar *tmpaltar[MAX_OF_TYPE];
lad *tmplad[MAX_OF_TYPE];
stair *tmpstair[MAX_OF_TYPE];
gold *tmpgold[MAX_OF_TYPE];
engraving *tmpengraving[MAX_OF_TYPE];
fountain *tmpfountain[MAX_OF_TYPE];
sink *tmpsink[MAX_OF_TYPE];
pool *tmppool[MAX_OF_TYPE];

mazepart *tmppart[10];
room *tmproom[MAXNROFROOMS*2];
corridor *tmpcor[MAX_OF_TYPE];

static specialmaze maze;
static splev special_lev;
static lev_init init_lev;

static char olist[MAX_REGISTERS], mlist[MAX_REGISTERS];
static struct coord plist[MAX_REGISTERS];

int n_olist = 0, n_mlist = 0, n_plist = 0;

unsigned int nlreg = 0, nreg = 0, ndoor = 0, ntrap = 0, nmons = 0, nobj = 0;
unsigned int ndb = 0, nwalk = 0, npart = 0, ndig = 0, nlad = 0, nstair = 0;
unsigned int naltar = 0, ncorridor = 0, nrooms = 0, ngold = 0, nengraving = 0;
unsigned int nfountain = 0, npool = 0, nsink = 0, npass = 0;

static int lev_flags = 0;

unsigned int max_x_map, max_y_map;

static xchar in_room;

extern int fatal_error;
extern int want_warnings;
extern const char *fname;


#line 131 "lev_comp.y"
typedef union
{
	int	i;
	char*	map;
	struct {
		xchar room;
		xchar wall;
		xchar door;
	} corpos;
} YYSTYPE;
#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		476
#define	YYFLAG		-32768
#define	YYNTBASE	77

#define YYTRANSLATE(x) ((unsigned)(x) <= 325 ? yytranslate[x] : 195)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,    71,
    72,     2,     2,    69,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,    70,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
    73,     2,    74,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
    26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
    36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
    46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
    56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
    66,    67,    68,    75,    76
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     1,     3,     5,     8,    10,    12,    18,    26,    30,
    31,    45,    47,    49,    50,    54,    58,    60,    61,    64,
    68,    69,    72,    76,    80,    81,    83,    85,    88,    90,
    92,    94,    95,    98,   104,   110,   118,   121,   124,   137,
   150,   151,   154,   160,   162,   168,   170,   176,   178,   184,
   186,   187,   190,   192,   194,   196,   198,   200,   202,   204,
   206,   208,   210,   212,   214,   216,   220,   224,   234,   236,
   238,   240,   242,   244,   246,   252,   254,   256,   258,   261,
   265,   267,   270,   276,   278,   280,   282,   284,   285,   288,
   292,   296,   300,   302,   306,   308,   312,   314,   315,   320,
   321,   324,   326,   328,   330,   332,   334,   336,   338,   340,
   342,   344,   346,   348,   350,   352,   354,   356,   358,   360,
   362,   364,   365,   376,   377,   380,   383,   386,   389,   392,
   396,   399,   402,   403,   413,   415,   417,   418,   426,   432,
   438,   440,   442,   444,   446,   448,   449,   452,   455,   461,
   468,   476,   482,   484,   490,   496,   497,   506,   507,   516,
   517,   518,   527,   528,   535,   536,   539,   541,   552,   556,
   560,   564,   568,   572,   581,   589,   595,   603,   605,   607,
   609,   611,   613,   615,   617,   619,   621,   623,   625,   627,
   629,   631,   632,   635,   640,   642,   644,   646,   648,   650,
   652,   654,   656,   658,   660,   662,   664,   669,   674,   679,
   684,   686,   688,   690,   692,   694,   696,   697,   699,   701,
   703,   709
};

static const short yyrhs[] = {    -1,
    78,     0,    79,     0,    79,    78,     0,    80,     0,    81,
     0,   114,    85,    83,    87,   116,     0,    82,    85,    83,
    87,    89,    91,    93,     0,     9,    70,   189,     0,     0,
    10,    70,     3,    69,     3,    69,     5,    69,     5,    69,
   179,    69,    84,     0,     5,     0,    50,     0,     0,    63,
    70,    86,     0,    64,    69,    86,     0,    64,     0,     0,
    88,    87,     0,     7,    70,    75,     0,     0,    89,    90,
     0,    23,    70,   124,     0,    24,    70,   125,     0,     0,
    92,     0,    98,     0,    98,    92,     0,    94,     0,    95,
     0,    44,     0,     0,    95,    96,     0,    37,    70,    97,
    69,    97,     0,    37,    70,    97,    69,     4,     0,    71,
     4,    69,    49,    69,   113,    72,     0,   100,   106,     0,
    99,   106,     0,    61,    70,   175,    69,   179,    69,   103,
    69,   105,    69,   189,   101,     0,    31,    70,   175,    69,
   179,    69,   102,    69,   104,    69,   105,   101,     0,     0,
    69,     5,     0,    71,     4,    69,     4,    72,     0,    50,
     0,    71,     4,    69,     4,    72,     0,    50,     0,    71,
   120,    69,   121,    72,     0,    50,     0,    71,     4,    69,
     4,    72,     0,    50,     0,     0,   106,   107,     0,   108,
     0,   109,     0,   110,     0,   130,     0,   134,     0,   144,
     0,   167,     0,   161,     0,   162,     0,   163,     0,   168,
     0,   169,     0,   149,     0,    62,    70,   189,     0,    36,
    70,     4,     0,    17,    70,   111,    69,   178,    69,   112,
    69,   113,     0,     5,     0,    50,     0,    49,     0,    50,
     0,     4,     0,    50,     0,     8,    70,   189,    69,   115,
     0,     3,     0,    50,     0,   117,     0,   117,   116,     0,
   118,   122,   128,     0,    12,     0,   119,    76,     0,    11,
    70,   120,    69,   121,     0,    56,     0,    57,     0,    58,
     0,    57,     0,     0,   122,   123,     0,    23,    70,   124,
     0,    25,    70,   126,     0,    24,    70,   125,     0,   188,
     0,   188,    69,   124,     0,   187,     0,   187,    69,   125,
     0,   186,     0,     0,   186,   127,    69,   126,     0,     0,
   128,   129,     0,   130,     0,   134,     0,   143,     0,   144,
     0,   145,     0,   166,     0,   150,     0,   152,     0,   154,
     0,   157,     0,   167,     0,   161,     0,   146,     0,   147,
     0,   148,     0,   149,     0,   168,     0,   169,     0,   164,
     0,   165,     0,     0,    15,   191,    70,   170,    69,   172,
    69,   177,   131,   132,     0,     0,   132,   133,     0,    69,
   189,     0,    69,    65,     0,    69,    66,     0,    69,   180,
     0,    69,    67,   189,     0,    13,   135,     0,    14,   135,
     0,     0,   191,    70,   171,    69,   173,   136,    69,   137,
   138,     0,   177,     0,    68,     0,     0,    69,   139,    69,
   140,    69,   141,   142,     0,    69,   139,    69,   141,   142,
     0,    69,   140,    69,   141,   142,     0,    50,     0,    47,
     0,    75,     0,    50,     0,     4,     0,     0,    69,    43,
     0,    69,    75,     0,    17,    70,   178,    69,   177,     0,
    16,   191,    70,   174,    69,   177,     0,    18,    70,   177,
    69,    49,    69,   178,     0,    19,    70,   177,    69,    49,
     0,    20,     0,    27,    70,   177,    69,    60,     0,    28,
    70,   177,    69,    60,     0,     0,    28,    70,   160,   151,
    69,   160,    69,    60,     0,     0,    32,    70,   160,   153,
    69,   160,    69,   189,     0,     0,     0,    33,    70,   160,
   155,    69,   160,   156,   159,     0,     0,    34,    70,   160,
   158,    69,   160,     0,     0,    69,    60,     0,   194,     0,
    35,    71,     4,    69,     4,    69,     4,    69,     4,    72,
     0,    40,    70,   177,     0,    42,    70,   177,     0,    41,
    70,   177,     0,    29,    70,   194,     0,    30,    70,   194,
     0,    21,    70,   194,    69,   179,    69,   175,   176,     0,
    26,    70,   177,    69,   180,    69,   181,     0,    38,    70,
   190,    69,   177,     0,    39,    70,   177,    69,   192,    69,
   189,     0,   187,     0,    50,     0,   184,     0,   188,     0,
    50,     0,   183,     0,   189,     0,    50,     0,   189,     0,
    50,     0,   189,     0,    50,     0,   189,     0,    50,     0,
     0,    69,    22,     0,    69,    22,    69,     5,     0,   193,
     0,   182,     0,    50,     0,    45,     0,    50,     0,    46,
     0,    50,     0,    55,     0,   185,     0,    50,     0,    59,
     0,    50,     0,    53,    73,     4,    74,     0,    51,    73,
     4,    74,     0,    52,    73,     4,    74,     0,    54,    73,
     4,    74,     0,   193,     0,     3,     0,     3,     0,    75,
     0,     4,     0,    50,     0,     0,     6,     0,    48,     0,
    50,     0,    71,     4,    69,     4,    72,     0,    71,     4,
    69,     4,    69,     4,    69,     4,    72,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
   169,   170,   173,   174,   177,   178,   181,   208,   245,   257,
   266,   283,   284,   287,   291,   298,   302,   308,   309,   312,
   329,   330,   333,   344,   357,   372,   375,   376,   379,   380,
   383,   391,   392,   395,   410,   426,   436,   440,   446,   464,
   482,   486,   492,   502,   508,   517,   523,   528,   534,   539,
   545,   546,   549,   550,   551,   552,   553,   554,   555,   556,
   557,   558,   559,   560,   561,   564,   573,   586,   606,   607,
   610,   611,   614,   615,   618,   631,   635,   641,   642,   645,
   651,   667,   680,   686,   687,   690,   691,   694,   695,   698,
   709,   724,   737,   744,   753,   760,   769,   776,   783,   786,
   787,   790,   791,   792,   793,   794,   795,   796,   797,   798,
   799,   800,   801,   802,   803,   804,   805,   806,   807,   808,
   809,   812,   840,   848,   849,   852,   856,   860,   864,   868,
   875,   878,   887,   906,   915,   924,   933,   943,   946,   949,
   954,   958,   964,   975,   979,   985,   986,   989,   995,  1013,
  1030,  1072,  1086,  1092,  1109,  1126,  1135,  1155,  1164,  1181,
  1190,  1199,  1214,  1223,  1240,  1244,  1250,  1254,  1274,  1290,
  1303,  1316,  1331,  1346,  1412,  1430,  1447,  1465,  1466,  1470,
  1473,  1474,  1478,  1481,  1482,  1488,  1489,  1495,  1503,  1506,
  1516,  1519,  1523,  1527,  1533,  1534,  1535,  1541,  1542,  1545,
  1546,  1549,  1550,  1551,  1557,  1558,  1561,  1570,  1579,  1588,
  1597,  1600,  1611,  1623,  1626,  1627,  1630,  1634,  1642,  1643,
  1646,  1657
};
#endif


#if YYDEBUG != 0 || defined (YYERROR_VERBOSE)

static const char * const yytname[] = {   "$","error","$undefined.","CHAR","INTEGER",
"BOOLEAN","PERCENT","MESSAGE_ID","MAZE_ID","LEVEL_ID","LEV_INIT_ID","GEOMETRY_ID",
"NOMAP_ID","OBJECT_ID","COBJECT_ID","MONSTER_ID","TRAP_ID","DOOR_ID","DRAWBRIDGE_ID",
"MAZEWALK_ID","WALLIFY_ID","REGION_ID","FILLING","RANDOM_OBJECTS_ID","RANDOM_MONSTERS_ID",
"RANDOM_PLACES_ID","ALTAR_ID","LADDER_ID","STAIR_ID","NON_DIGGABLE_ID","NON_PASSWALL_ID",
"ROOM_ID","PORTAL_ID","TELEPRT_ID","BRANCH_ID","LEV","CHANCE_ID","CORRIDOR_ID",
"GOLD_ID","ENGRAVING_ID","FOUNTAIN_ID","POOL_ID","SINK_ID","NONE","RAND_CORRIDOR_ID",
"DOOR_STATE","LIGHT_STATE","CURSE_TYPE","ENGRAVING_TYPE","DIRECTION","RANDOM_TYPE",
"O_REGISTER","M_REGISTER","P_REGISTER","A_REGISTER","ALIGNMENT","LEFT_OR_RIGHT",
"CENTER","TOP_OR_BOT","ALTAR_TYPE","UP_OR_DOWN","SUBROOM_ID","NAME_ID","FLAGS_ID",
"FLAG_TYPE","MON_ATTITUDE","MON_ALERTNESS","MON_APPEARANCE","CONTAINED","','",
"':'","'('","')'","'['","']'","STRING","MAP_ID","file","levels","level","maze_level",
"room_level","level_def","lev_init","walled","flags","flag_list","messages",
"message","rreg_init","init_rreg","rooms","roomlist","corridors_def","random_corridors",
"corridors","corridor","corr_spec","aroom","subroom_def","room_def","roomfill",
"room_pos","subroom_pos","room_align","room_size","room_details","room_detail",
"room_name","room_chance","room_door","secret","door_wall","door_pos","maze_def",
"filling","regions","aregion","map_definition","map_geometry","h_justif","v_justif",
"reg_init","init_reg","object_list","monster_list","place_list","@1","map_details",
"map_detail","monster_detail","@2","monster_infos","monster_info","object_detail",
"object_desc","@3","object_where","object_infos","curse_state","monster_id",
"enchantment","optional_name","door_detail","trap_detail","drawbridge_detail",
"mazewalk_detail","wallify_detail","ladder_detail","stair_detail","stair_region",
"@4","portal_region","@5","teleprt_region","@6","@7","branch_region","@8","teleprt_detail",
"lev_region","fountain_detail","sink_detail","pool_detail","diggable_detail",
"passwall_detail","region_detail","altar_detail","gold_detail","engraving_detail",
"monster_c","object_c","m_name","o_name","trap_name","room_type","prefilled",
"coordinate","door_state","light_state","alignment","altar_type","p_register",
"o_register","m_register","a_register","place","monster","object","string","amount",
"chance","engraving_type","coord","region", NULL
};
#endif

static const short yyr1[] = {     0,
    77,    77,    78,    78,    79,    79,    80,    81,    82,    83,
    83,    84,    84,    85,    85,    86,    86,    87,    87,    88,
    89,    89,    90,    90,    91,    91,    92,    92,    93,    93,
    94,    95,    95,    96,    96,    97,    98,    98,    99,   100,
   101,   101,   102,   102,   103,   103,   104,   104,   105,   105,
   106,   106,   107,   107,   107,   107,   107,   107,   107,   107,
   107,   107,   107,   107,   107,   108,   109,   110,   111,   111,
   112,   112,   113,   113,   114,   115,   115,   116,   116,   117,
   118,   118,   119,   120,   120,   121,   121,   122,   122,   123,
   123,   123,   124,   124,   125,   125,   126,   127,   126,   128,
   128,   129,   129,   129,   129,   129,   129,   129,   129,   129,
   129,   129,   129,   129,   129,   129,   129,   129,   129,   129,
   129,   131,   130,   132,   132,   133,   133,   133,   133,   133,
   134,   134,   136,   135,   137,   137,   138,   138,   138,   138,
   139,   139,   140,   141,   141,   142,   142,   142,   143,   144,
   145,   146,   147,   148,   149,   151,   150,   153,   152,   155,
   156,   154,   158,   157,   159,   159,   160,   160,   161,   162,
   163,   164,   165,   166,   167,   168,   169,   170,   170,   170,
   171,   171,   171,   172,   172,   173,   173,   174,   174,   175,
   175,   176,   176,   176,   177,   177,   177,   178,   178,   179,
   179,   180,   180,   180,   181,   181,   182,   183,   184,   185,
   186,   187,   188,   189,   190,   190,   191,   191,   192,   192,
   193,   194
};

static const short yyr2[] = {     0,
     0,     1,     1,     2,     1,     1,     5,     7,     3,     0,
    13,     1,     1,     0,     3,     3,     1,     0,     2,     3,
     0,     2,     3,     3,     0,     1,     1,     2,     1,     1,
     1,     0,     2,     5,     5,     7,     2,     2,    12,    12,
     0,     2,     5,     1,     5,     1,     5,     1,     5,     1,
     0,     2,     1,     1,     1,     1,     1,     1,     1,     1,
     1,     1,     1,     1,     1,     3,     3,     9,     1,     1,
     1,     1,     1,     1,     5,     1,     1,     1,     2,     3,
     1,     2,     5,     1,     1,     1,     1,     0,     2,     3,
     3,     3,     1,     3,     1,     3,     1,     0,     4,     0,
     2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
     1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
     1,     0,    10,     0,     2,     2,     2,     2,     2,     3,
     2,     2,     0,     9,     1,     1,     0,     7,     5,     5,
     1,     1,     1,     1,     1,     0,     2,     2,     5,     6,
     7,     5,     1,     5,     5,     0,     8,     0,     8,     0,
     0,     8,     0,     6,     0,     2,     1,    10,     3,     3,
     3,     3,     3,     8,     7,     5,     7,     1,     1,     1,
     1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
     1,     0,     2,     4,     1,     1,     1,     1,     1,     1,
     1,     1,     1,     1,     1,     1,     4,     4,     4,     4,
     1,     1,     1,     1,     1,     1,     0,     1,     1,     1,
     5,     9
};

static const short yydefact[] = {     1,
     0,     0,     2,     3,     5,     6,    14,    14,     0,     0,
     4,     0,    10,    10,   214,     0,     9,     0,     0,    18,
    18,     0,    17,    15,     0,     0,    21,    18,     0,    76,
    77,    75,     0,     0,     0,    25,    19,     0,    81,     7,
    78,    88,     0,    16,     0,    20,     0,     0,     0,     0,
    22,    32,    26,    27,    51,    51,     0,    79,   100,    82,
     0,     0,     0,     0,     0,    31,     8,    29,    30,    28,
    38,    37,    84,    85,     0,     0,     0,     0,    89,    80,
     0,   213,    23,    93,   212,    24,    95,   191,     0,   190,
     0,     0,    33,   217,   217,   217,   217,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,    52,    53,    54,
    55,    56,    57,    58,    65,    60,    61,    62,    59,    63,
    64,     0,     0,     0,     0,     0,     0,     0,   153,     0,
     0,     0,     0,     0,     0,     0,     0,   101,   102,   103,
   104,   105,   106,   114,   115,   116,   117,   108,   109,   110,
   111,   113,   120,   121,   107,   112,   118,   119,     0,     0,
     0,     0,     0,     0,   218,   131,     0,   132,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    87,    86,    83,    90,    92,     0,    91,    97,   211,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,    94,    96,   200,   201,     0,     0,     0,     0,     0,
     0,     0,    69,    70,     0,   197,     0,     0,   196,   195,
     0,    67,   215,   216,     0,     0,   169,   171,   170,    66,
     0,     0,   198,   199,     0,     0,     0,     0,     0,     0,
     0,     0,   156,   167,   172,   173,   158,   160,   163,     0,
     0,     0,     0,     0,   182,     0,     0,   183,   181,   179,
     0,     0,   180,   178,   189,     0,   188,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,    44,     0,
     0,    46,     0,     0,     0,    35,    34,     0,     0,     0,
     0,     0,     0,     0,   204,     0,   202,     0,   203,   155,
   176,   219,   220,     0,     0,    99,   149,     0,   152,     0,
     0,   154,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,   187,   133,   186,     0,   185,
     0,   184,   150,     0,   207,     0,     0,     0,   221,     0,
     0,     0,     0,     0,     0,     0,   161,   164,     0,     0,
    48,     0,     0,     0,    50,     0,     0,     0,   208,     0,
   209,     0,    71,    72,     0,     0,   206,   205,   175,   177,
   151,     0,   192,     0,     0,     0,   165,    12,    13,    11,
     0,     0,     0,     0,     0,     0,    73,    74,     0,     0,
   122,     0,   210,     0,     0,   174,     0,   157,   159,     0,
   162,    43,     0,    41,    45,     0,    41,    36,   136,   137,
   135,   124,    68,     0,   193,     0,   166,     0,     0,    40,
     0,    39,     0,   134,   123,     0,     0,     0,    47,    42,
    49,   142,   141,   143,     0,     0,     0,   125,   222,   194,
     0,     0,     0,   127,   128,     0,   129,   126,   168,   145,
   144,     0,   146,   146,   130,     0,     0,   139,   140,   146,
   147,   148,   138,     0,     0,     0
};

static const short yydefgoto[] = {   474,
     3,     4,     5,     6,     7,    20,   390,    13,    24,    27,
    28,    36,    51,    52,    53,    67,    68,    69,    93,   209,
    54,    55,    56,   430,   291,   294,   363,   367,    71,   108,
   109,   110,   111,   215,   375,   399,     8,    32,    40,    41,
    42,    43,    75,   183,    59,    79,    83,    86,   187,   232,
    80,   138,   112,   422,   435,   448,   113,   166,   370,   420,
   434,   445,   446,   463,   468,   141,   114,   143,   144,   145,
   146,   115,   148,   284,   149,   285,   150,   286,   387,   151,
   287,   411,   243,   116,   117,   118,   153,   154,   155,   119,
   120,   121,   262,   257,   341,   337,   266,    89,   406,   221,
   235,   206,   308,   379,   219,   258,   263,   309,   188,    87,
    84,    90,   225,   167,   314,   220,   244
};

static const short yypact[] = {   157,
   -13,    33,-32768,   157,-32768,-32768,    46,    46,    44,    44,
-32768,    68,   145,   145,-32768,    92,-32768,   103,   113,   175,
   175,    22,   115,-32768,   182,   116,-32768,   175,   161,-32768,
-32768,-32768,   103,   120,   117,    28,-32768,   121,-32768,-32768,
   161,-32768,   111,-32768,   185,-32768,   123,   124,   125,   126,
-32768,   146,-32768,     6,-32768,-32768,   118,-32768,   112,-32768,
   128,   195,   196,   -11,   -11,-32768,-32768,-32768,   163,-32768,
     4,     4,-32768,-32768,   132,   135,   136,   137,-32768,   130,
   197,-32768,-32768,   139,-32768,-32768,   140,-32768,   141,-32768,
   142,   143,-32768,   206,   206,   206,   206,   144,   147,   148,
   149,   150,   151,   152,   153,   154,   155,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,   119,   195,   196,   156,   158,   159,   160,-32768,   164,
   165,   166,   167,   168,   169,   170,   171,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,   162,   195,
   196,    81,    81,   172,-32768,-32768,   174,-32768,   176,   177,
    26,    47,    47,   211,    23,    47,    47,    47,    47,    44,
-32768,-32768,-32768,-32768,-32768,   212,-32768,   173,-32768,    78,
    47,    47,   178,    47,    27,   178,   178,     0,     0,     0,
   221,-32768,-32768,-32768,-32768,   179,   181,   229,   183,    19,
    13,    10,-32768,-32768,   184,-32768,   186,   187,-32768,-32768,
   188,-32768,-32768,-32768,   189,   191,-32768,-32768,-32768,-32768,
   192,   193,-32768,-32768,   194,   198,   199,   241,   200,   201,
   180,   250,-32768,-32768,-32768,-32768,-32768,-32768,-32768,   202,
   -17,    31,   203,    11,-32768,   204,   205,-32768,-32768,-32768,
   207,   209,-32768,-32768,-32768,   210,-32768,    78,   251,    71,
   213,    47,   104,   260,   156,    47,   216,   217,   214,    81,
   215,   272,   218,   219,   222,   224,   225,    81,-32768,   277,
   226,-32768,   278,   227,   235,-32768,-32768,   281,    37,   282,
    41,    47,   228,   230,-32768,   232,-32768,   231,-32768,-32768,
-32768,-32768,-32768,   233,   234,-32768,-32768,   238,-32768,   294,
   239,-32768,   240,   295,     0,     0,     0,     0,   242,   243,
    42,   244,    49,   245,   236,-32768,-32768,-32768,   246,-32768,
   247,-32768,-32768,   129,-32768,   297,    51,    44,-32768,    78,
   248,   -11,   299,   -31,   249,   252,-32768,-32768,    29,   311,
-32768,   118,   253,   315,-32768,   319,   255,    24,-32768,   256,
-32768,    47,-32768,-32768,   257,   254,-32768,-32768,-32768,-32768,
-32768,   323,   261,   262,   269,    44,   263,-32768,-32768,-32768,
   264,   265,    49,   266,   268,    44,-32768,-32768,   270,    43,
-32768,    24,-32768,   271,   313,-32768,   329,-32768,-32768,   283,
-32768,-32768,   119,   275,-32768,   337,   275,-32768,-32768,   276,
-32768,-32768,-32768,   342,   279,   280,-32768,   284,   345,-32768,
   285,-32768,     8,-32768,   286,   287,   346,   343,-32768,-32768,
-32768,-32768,-32768,-32768,   289,   291,    40,-32768,-32768,-32768,
   290,     3,    25,-32768,-32768,    44,-32768,-32768,-32768,-32768,
-32768,   292,   296,   296,-32768,    25,    -7,-32768,-32768,   296,
-32768,-32768,-32768,   352,   353,-32768
};

static const short yypgoto[] = {-32768,
   350,-32768,-32768,-32768,-32768,   349,-32768,   356,   333,    96,
-32768,-32768,-32768,-32768,   314,-32768,-32768,-32768,-32768,   122,
-32768,-32768,-32768,   -50,-32768,-32768,-32768,   -24,   316,-32768,
-32768,-32768,-32768,-32768,-32768,   -32,-32768,-32768,   330,-32768,
-32768,-32768,    12,   -40,-32768,-32768,   -76,   -75,   100,-32768,
-32768,-32768,   298,-32768,-32768,-32768,   300,   288,-32768,-32768,
-32768,-32768,   -73,  -405,  -414,-32768,   301,-32768,-32768,-32768,
-32768,   302,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,  -186,   304,-32768,-32768,-32768,-32768,-32768,   305,
   306,   308,-32768,-32768,-32768,-32768,-32768,   -63,-32768,  -168,
  -262,  -158,   -58,-32768,-32768,-32768,-32768,-32768,-32768,   190,
   208,    -9,-32768,    84,-32768,  -122,   -64
};


#define	YYLAST		447


static const short yytable[] = {    16,
    17,    91,   189,   218,   207,   303,   460,   226,   227,   228,
   229,   247,   248,   249,   296,    85,    94,    95,    96,    97,
    98,    82,   236,   237,    30,   240,   223,   397,   460,    99,
   213,   100,   289,   388,   241,   471,    49,   382,    88,   101,
   349,   102,   103,   104,   105,   106,   184,   464,   185,   469,
    47,    48,   461,   290,   442,   473,     9,   443,    49,   265,
   470,   241,   260,    15,   261,   107,    50,   472,   255,   256,
   238,    31,   224,   398,   461,   214,   216,   444,   389,   217,
   292,   208,   444,   202,    15,   203,   336,   381,    50,   305,
   340,   361,   216,   306,   307,   217,   216,   242,   365,   217,
   377,   293,    10,   311,   454,   455,   456,   317,    12,   378,
   419,    15,   362,   186,    15,    15,    29,   186,    15,   366,
   305,   321,   233,    37,   306,   307,   204,   234,   239,   329,
   205,   245,   246,   343,    76,    77,    78,    18,   355,   356,
   357,   358,    94,    95,    96,    97,   126,   127,   128,   129,
   130,   312,   189,   313,    19,    99,   131,   132,   133,   134,
    22,   135,   136,   137,     1,     2,    23,   102,   103,   104,
   230,    38,    39,    73,    74,   181,   182,   373,   374,   169,
   170,    26,    25,    33,    34,    35,    60,    61,    45,    66,
    57,    46,    62,    63,    64,    65,    81,    82,    85,    92,
   122,   159,   267,   401,   123,   124,   125,   160,   161,   162,
   163,   165,   164,   171,   222,   231,   172,   173,   174,   175,
   176,   177,   178,   179,   180,   250,   186,   190,   191,   192,
   201,   421,   253,   193,   194,   195,   196,   197,   198,   199,
   200,   -98,   208,   210,   279,   211,   212,   251,   238,   252,
   282,   254,   268,   283,   304,   270,   271,   272,   269,   273,
   274,   275,   276,   315,   318,   319,   277,   278,   280,   281,
   288,   295,   310,   299,   322,   323,   298,   301,   302,   300,
   330,   332,   320,   334,   335,   339,   324,   325,   383,   338,
   326,   342,   327,   328,   331,   333,   344,   351,   354,   347,
   376,   348,   384,   345,   346,   349,   350,   352,   353,   369,
   359,   360,   364,   368,   391,   372,   382,   385,   394,   371,
   386,   393,   395,   396,   400,   402,   404,   403,   408,   405,
   407,   410,   426,   413,   425,   412,   416,   415,   380,   424,
   431,   418,   427,   429,   433,   436,   451,   437,   438,   440,
   450,   475,   476,    11,   447,   439,   441,   452,   449,   453,
   466,   459,    21,    14,   467,    44,   432,    70,   414,   423,
    58,    72,   428,   392,   316,   297,   409,   139,   462,   140,
   142,   147,   168,   152,   156,   157,   417,   158,   457,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
   264,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,   259,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,   458,     0,     0,
     0,     0,     0,     0,     0,     0,   465
};

static const short yycheck[] = {     9,
    10,    65,   125,   172,   163,   268,     4,   176,   177,   178,
   179,   198,   199,   200,     4,     3,    13,    14,    15,    16,
    17,     3,   191,   192,     3,   194,     4,     4,     4,    26,
     5,    28,    50,     5,    35,    43,    31,    69,    50,    36,
    72,    38,    39,    40,    41,    42,   123,   453,   124,   464,
    23,    24,    50,    71,    47,   470,    70,    50,    31,    50,
   466,    35,    50,    75,    52,    62,    61,    75,    50,    51,
    71,    50,    50,    50,    50,    50,    50,    75,    50,    53,
    50,    71,    75,   160,    75,   161,    50,   350,    61,    50,
    50,    50,    50,    54,    55,    53,    50,    71,    50,    53,
    50,    71,    70,   272,    65,    66,    67,   276,    63,    59,
    68,    75,    71,    71,    75,    75,    21,    71,    75,    71,
    50,   280,    45,    28,    54,    55,    46,    50,   193,   288,
    50,   196,   197,   302,    23,    24,    25,    70,   325,   326,
   327,   328,    13,    14,    15,    16,    17,    18,    19,    20,
    21,    48,   275,    50,    10,    26,    27,    28,    29,    30,
    69,    32,    33,    34,     8,     9,    64,    38,    39,    40,
   180,    11,    12,    56,    57,    57,    58,    49,    50,    96,
    97,     7,    70,    69,     3,    70,    76,     3,    69,    44,
    70,    75,    70,    70,    70,    70,    69,     3,     3,    37,
    69,     5,   212,   372,    70,    70,    70,    69,    69,    69,
    69,     6,    70,    70,     4,     4,    70,    70,    70,    70,
    70,    70,    70,    70,    70,     5,    71,    70,    70,    70,
    69,   400,     4,    70,    70,    70,    70,    70,    70,    70,
    70,    69,    71,    70,     4,    70,    70,    69,    71,    69,
    71,    69,    69,     4,     4,    69,    69,    69,    73,    69,
    69,    69,    69,     4,    49,    49,    69,    69,    69,    69,
    69,    69,    60,    69,    60,     4,    73,    69,    69,    73,
     4,     4,    69,    49,     4,     4,    69,    69,   352,   299,
    69,   301,    69,    69,    69,    69,    69,     4,     4,    69,
     4,    69,     4,    74,    73,    72,    69,    69,    69,    74,
    69,    69,    69,    69,     4,    69,    69,    69,     4,    74,
    69,    69,     4,    69,    69,    69,     4,    74,    60,    69,
    69,    69,     4,    69,    22,    72,    69,    72,   348,    69,
     4,    72,    60,    69,    69,     4,     4,    69,    69,     5,
     5,     0,     0,     4,    69,    72,    72,    69,    72,    69,
    69,    72,    14,     8,    69,    33,   417,    54,   393,   402,
    41,    56,   413,   362,   275,   254,   386,    80,   452,    80,
    80,    80,    95,    80,    80,    80,   396,    80,   447,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
   211,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,   210,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,   447,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,   456
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/usr/lib/bison.simple"

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi)
#include <alloca.h>
#else /* not sparc */
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#else /* not MSDOS, __TURBOC__, or _AIX */
#ifdef __hpux
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca ();
#endif /* not __cplusplus */
#endif /* __hpux */
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
int yyparse (void);
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(TO,FROM,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (to, from, count)
     char *to;
     char *from;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *to, char *from, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 196 "/usr/lib/bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#ifdef __cplusplus
#define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#else /* not __cplusplus */
#define YYPARSE_PARAM_ARG YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#endif /* not __cplusplus */
#else /* not YYPARSE_PARAM */
#define YYPARSE_PARAM_ARG
#define YYPARSE_PARAM_DECL
#endif /* not YYPARSE_PARAM */

int
yyparse(YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
      yyss = (short *) alloca (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss, (char *)yyss1, size * sizeof (*yyssp));
      yyvs = (YYSTYPE *) alloca (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs, (char *)yyvs1, size * sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) alloca (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls, (char *)yyls1, size * sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 7:
#line 182 "lev_comp.y"
{
			unsigned i;

			if (fatal_error > 0) {
				(void) fprintf(stderr,
				"%s : %d errors detected. No output created!\n",
					fname, fatal_error);
			} else {
				maze.flags = yyvsp[-3].i;
				(void) memcpy((genericptr_t)&(maze.init_lev),
						(genericptr_t)&(init_lev),
						sizeof(lev_init));
				maze.numpart = npart;
				maze.parts = NewTab(mazepart, npart);
				for(i=0;i<npart;i++)
				    maze.parts[i] = tmppart[i];
				if (!write_level_file(yyvsp[-4].map, (splev *)0, &maze)) {
					yyerror("Can't open output file!!");
					exit(EXIT_FAILURE);
				}
				npart = 0;
			}
			Free(yyvsp[-4].map);
		  ;
    break;}
case 8:
#line 209 "lev_comp.y"
{
			unsigned i;

			if (fatal_error > 0) {
			    (void) fprintf(stderr,
			      "%s : %d errors detected. No output created!\n",
					fname, fatal_error);
			} else {
				special_lev.flags = (long) yyvsp[-5].i;
				(void) memcpy(
					(genericptr_t)&(special_lev.init_lev),
					(genericptr_t)&(init_lev),
					sizeof(lev_init));
				special_lev.nroom = nrooms;
				special_lev.rooms = NewTab(room, nrooms);
				for(i=0; i<nrooms; i++)
				    special_lev.rooms[i] = tmproom[i];
				special_lev.ncorr = ncorridor;
				special_lev.corrs = NewTab(corridor, ncorridor);
				for(i=0; i<ncorridor; i++)
				    special_lev.corrs[i] = tmpcor[i];
				if (check_subrooms()) {
				    if (!write_level_file(yyvsp[-6].map, &special_lev,
							  (specialmaze *)0)) {
					yyerror("Can't open output file!!");
					exit(EXIT_FAILURE);
				    }
				}
				free_rooms(&special_lev);
				nrooms = 0;
				ncorridor = 0;
			}
			Free(yyvsp[-6].map);
		  ;
    break;}
case 9:
#line 246 "lev_comp.y"
{
			if (index(yyvsp[0].map, '.'))
			    yyerror("Invalid dot ('.') in level name.");
			if ((int) strlen(yyvsp[0].map) > 8)
			    yyerror("Level names limited to 8 characters.");
			yyval.map = yyvsp[0].map;
			special_lev.nrmonst = special_lev.nrobjects = 0;
			n_mlist = n_olist = 0;
		  ;
    break;}
case 10:
#line 258 "lev_comp.y"
{
			/* in case we're processing multiple files,
			   explicitly clear any stale settings */
			(void) memset((genericptr_t) &init_lev, 0,
					sizeof init_lev);
			init_lev.init_present = FALSE;
			yyval.i = 0;
		  ;
    break;}
case 11:
#line 267 "lev_comp.y"
{
			init_lev.init_present = TRUE;
			init_lev.fg = what_map_char((char) yyvsp[-10].i);
			if (init_lev.fg == INVALID_TYPE)
			    yyerror("Invalid foreground type.");
			init_lev.bg = what_map_char((char) yyvsp[-8].i);
			if (init_lev.bg == INVALID_TYPE)
			    yyerror("Invalid background type.");
			init_lev.smoothed = yyvsp[-6].i;
			init_lev.joined = yyvsp[-4].i;
			init_lev.lit = yyvsp[-2].i;
			init_lev.walled = yyvsp[0].i;
			yyval.i = 1;
		  ;
    break;}
case 14:
#line 288 "lev_comp.y"
{
			yyval.i = 0;
		  ;
    break;}
case 15:
#line 292 "lev_comp.y"
{
			yyval.i = lev_flags;
			lev_flags = 0;	/* clear for next user */
		  ;
    break;}
case 16:
#line 299 "lev_comp.y"
{
			lev_flags |= yyvsp[-2].i;
		  ;
    break;}
case 17:
#line 303 "lev_comp.y"
{
			lev_flags |= yyvsp[0].i;
		  ;
    break;}
case 20:
#line 313 "lev_comp.y"
{
			int i, j;

			i = (int) strlen(yyvsp[0].map) + 1;
			j = (int) strlen(tmpmessage);
			if (i + j > 255) {
			   yyerror("Message string too long (>256 characters)");
			} else {
			    if (j) tmpmessage[j++] = '\n';
			    (void) strncpy(tmpmessage+j, yyvsp[0].map, i - 1);
			    tmpmessage[j + i - 1] = 0;
			}
			Free(yyvsp[0].map);
		  ;
    break;}
case 23:
#line 334 "lev_comp.y"
{
			if(special_lev.nrobjects) {
			    yyerror("Object registers already initialized!");
			} else {
			    special_lev.nrobjects = n_olist;
			    special_lev.robjects = (char *) alloc(n_olist);
			    (void) memcpy((genericptr_t)special_lev.robjects,
					  (genericptr_t)olist, n_olist);
			}
		  ;
    break;}
case 24:
#line 345 "lev_comp.y"
{
			if(special_lev.nrmonst) {
			    yyerror("Monster registers already initialized!");
			} else {
			    special_lev.nrmonst = n_mlist;
			    special_lev.rmonst = (char *) alloc(n_mlist);
			    (void) memcpy((genericptr_t)special_lev.rmonst,
					  (genericptr_t)mlist, n_mlist);
			  }
		  ;
    break;}
case 25:
#line 358 "lev_comp.y"
{
			tmproom[nrooms] = New(room);
			tmproom[nrooms]->name = (char *) 0;
			tmproom[nrooms]->parent = (char *) 0;
			tmproom[nrooms]->rtype = 0;
			tmproom[nrooms]->rlit = 0;
			tmproom[nrooms]->xalign = ERR;
			tmproom[nrooms]->yalign = ERR;
			tmproom[nrooms]->x = 0;
			tmproom[nrooms]->y = 0;
			tmproom[nrooms]->w = 2;
			tmproom[nrooms]->h = 2;
			in_room = 1;
		  ;
    break;}
case 31:
#line 384 "lev_comp.y"
{
			tmpcor[0] = New(corridor);
			tmpcor[0]->src.room = -1;
			ncorridor = 1;
		  ;
    break;}
case 34:
#line 396 "lev_comp.y"
{
			tmpcor[ncorridor] = New(corridor);
			tmpcor[ncorridor]->src.room = yyvsp[-2].corpos.room;
			tmpcor[ncorridor]->src.wall = yyvsp[-2].corpos.wall;
			tmpcor[ncorridor]->src.door = yyvsp[-2].corpos.door;
			tmpcor[ncorridor]->dest.room = yyvsp[0].corpos.room;
			tmpcor[ncorridor]->dest.wall = yyvsp[0].corpos.wall;
			tmpcor[ncorridor]->dest.door = yyvsp[0].corpos.door;
			ncorridor++;
			if (ncorridor >= MAX_OF_TYPE) {
				yyerror("Too many corridors in level!");
				ncorridor--;
			}
		  ;
    break;}
case 35:
#line 411 "lev_comp.y"
{
			tmpcor[ncorridor] = New(corridor);
			tmpcor[ncorridor]->src.room = yyvsp[-2].corpos.room;
			tmpcor[ncorridor]->src.wall = yyvsp[-2].corpos.wall;
			tmpcor[ncorridor]->src.door = yyvsp[-2].corpos.door;
			tmpcor[ncorridor]->dest.room = -1;
			tmpcor[ncorridor]->dest.wall = yyvsp[0].i;
			ncorridor++;
			if (ncorridor >= MAX_OF_TYPE) {
				yyerror("Too many corridors in level!");
				ncorridor--;
			}
		  ;
    break;}
case 36:
#line 427 "lev_comp.y"
{
			if ((unsigned) yyvsp[-5].i >= nrooms)
			    yyerror("Wrong room number!");
			yyval.corpos.room = yyvsp[-5].i;
			yyval.corpos.wall = yyvsp[-3].i;
			yyval.corpos.door = yyvsp[-1].i;
		  ;
    break;}
case 37:
#line 437 "lev_comp.y"
{
			store_room();
		  ;
    break;}
case 38:
#line 441 "lev_comp.y"
{
			store_room();
		  ;
    break;}
case 39:
#line 447 "lev_comp.y"
{
			tmproom[nrooms] = New(room);
			tmproom[nrooms]->parent = yyvsp[-1].map;
			tmproom[nrooms]->name = (char *) 0;
			tmproom[nrooms]->rtype = yyvsp[-9].i;
			tmproom[nrooms]->rlit = yyvsp[-7].i;
			tmproom[nrooms]->filled = yyvsp[0].i;
			tmproom[nrooms]->xalign = ERR;
			tmproom[nrooms]->yalign = ERR;
			tmproom[nrooms]->x = current_coord.x;
			tmproom[nrooms]->y = current_coord.y;
			tmproom[nrooms]->w = current_size.width;
			tmproom[nrooms]->h = current_size.height;
			in_room = 1;
		  ;
    break;}
case 40:
#line 465 "lev_comp.y"
{
			tmproom[nrooms] = New(room);
			tmproom[nrooms]->name = (char *) 0;
			tmproom[nrooms]->parent = (char *) 0;
			tmproom[nrooms]->rtype = yyvsp[-9].i;
			tmproom[nrooms]->rlit = yyvsp[-7].i;
			tmproom[nrooms]->filled = yyvsp[0].i;
			tmproom[nrooms]->xalign = current_align.x;
			tmproom[nrooms]->yalign = current_align.y;
			tmproom[nrooms]->x = current_coord.x;
			tmproom[nrooms]->y = current_coord.y;
			tmproom[nrooms]->w = current_size.width;
			tmproom[nrooms]->h = current_size.height;
			in_room = 1;
		  ;
    break;}
case 41:
#line 483 "lev_comp.y"
{
			yyval.i = 1;
		  ;
    break;}
case 42:
#line 487 "lev_comp.y"
{
			yyval.i = yyvsp[0].i;
		  ;
    break;}
case 43:
#line 493 "lev_comp.y"
{
			if ( yyvsp[-3].i < 1 || yyvsp[-3].i > 5 ||
			    yyvsp[-1].i < 1 || yyvsp[-1].i > 5 ) {
			    yyerror("Room position should be between 1 & 5!");
			} else {
			    current_coord.x = yyvsp[-3].i;
			    current_coord.y = yyvsp[-1].i;
			}
		  ;
    break;}
case 44:
#line 503 "lev_comp.y"
{
			current_coord.x = current_coord.y = ERR;
		  ;
    break;}
case 45:
#line 509 "lev_comp.y"
{
			if ( yyvsp[-3].i < 0 || yyvsp[-1].i < 0) {
			    yyerror("Invalid subroom position !");
			} else {
			    current_coord.x = yyvsp[-3].i;
			    current_coord.y = yyvsp[-1].i;
			}
		  ;
    break;}
case 46:
#line 518 "lev_comp.y"
{
			current_coord.x = current_coord.y = ERR;
		  ;
    break;}
case 47:
#line 524 "lev_comp.y"
{
			current_align.x = yyvsp[-3].i;
			current_align.y = yyvsp[-1].i;
		  ;
    break;}
case 48:
#line 529 "lev_comp.y"
{
			current_align.x = current_align.y = ERR;
		  ;
    break;}
case 49:
#line 535 "lev_comp.y"
{
			current_size.width = yyvsp[-3].i;
			current_size.height = yyvsp[-1].i;
		  ;
    break;}
case 50:
#line 540 "lev_comp.y"
{
			current_size.height = current_size.width = ERR;
		  ;
    break;}
case 66:
#line 565 "lev_comp.y"
{
			if (tmproom[nrooms]->name)
			    yyerror("This room already has a name!");
			else
			    tmproom[nrooms]->name = yyvsp[0].map;
		  ;
    break;}
case 67:
#line 574 "lev_comp.y"
{
			if (tmproom[nrooms]->chance)
			    yyerror("This room already assigned a chance!");
			else if (tmproom[nrooms]->rtype == OROOM)
			    yyerror("Only typed rooms can have a chance!");
			else if (yyvsp[0].i < 1 || yyvsp[0].i > 99)
			    yyerror("The chance is supposed to be precentile.");
			else
			    tmproom[nrooms]->chance = yyvsp[0].i;
		   ;
    break;}
case 68:
#line 587 "lev_comp.y"
{
			/* ERR means random here */
			if (yyvsp[-2].i == ERR && yyvsp[0].i != ERR) {
		     yyerror("If the door wall is random, so must be its pos!");
			} else {
			    tmprdoor[ndoor] = New(room_door);
			    tmprdoor[ndoor]->secret = yyvsp[-6].i;
			    tmprdoor[ndoor]->mask = yyvsp[-4].i;
			    tmprdoor[ndoor]->wall = yyvsp[-2].i;
			    tmprdoor[ndoor]->pos = yyvsp[0].i;
			    ndoor++;
			    if (ndoor >= MAX_OF_TYPE) {
				    yyerror("Too many doors in room!");
				    ndoor--;
			    }
			}
		  ;
    break;}
case 75:
#line 619 "lev_comp.y"
{
			maze.filling = (schar) yyvsp[0].i;
			if (index(yyvsp[-2].map, '.'))
			    yyerror("Invalid dot ('.') in level name.");
			if ((int) strlen(yyvsp[-2].map) > 8)
			    yyerror("Level names limited to 8 characters.");
			yyval.map = yyvsp[-2].map;
			in_room = 0;
			n_plist = n_mlist = n_olist = 0;
		  ;
    break;}
case 76:
#line 632 "lev_comp.y"
{
			yyval.i = get_floor_type((char)yyvsp[0].i);
		  ;
    break;}
case 77:
#line 636 "lev_comp.y"
{
			yyval.i = -1;
		  ;
    break;}
case 80:
#line 646 "lev_comp.y"
{
			store_part();
		  ;
    break;}
case 81:
#line 652 "lev_comp.y"
{
			tmppart[npart] = New(mazepart);
			tmppart[npart]->halign = 1;
			tmppart[npart]->valign = 1;
			tmppart[npart]->nrobjects = 0;
			tmppart[npart]->nloc = 0;
			tmppart[npart]->nrmonst = 0;
			tmppart[npart]->xsize = 1;
			tmppart[npart]->ysize = 1;
			tmppart[npart]->map = (char **) alloc(sizeof(char *));
			tmppart[npart]->map[0] = (char *) alloc(1);
			tmppart[npart]->map[0][0] = STONE;
			max_x_map = COLNO-1;
			max_y_map = ROWNO;
		  ;
    break;}
case 82:
#line 668 "lev_comp.y"
{
			tmppart[npart] = New(mazepart);
			tmppart[npart]->halign = yyvsp[-1].i % 10;
			tmppart[npart]->valign = yyvsp[-1].i / 10;
			tmppart[npart]->nrobjects = 0;
			tmppart[npart]->nloc = 0;
			tmppart[npart]->nrmonst = 0;
			scan_map(yyvsp[0].map);
			Free(yyvsp[0].map);
		  ;
    break;}
case 83:
#line 681 "lev_comp.y"
{
			yyval.i = yyvsp[-2].i + (yyvsp[0].i * 10);
		  ;
    break;}
case 90:
#line 699 "lev_comp.y"
{
			if (tmppart[npart]->nrobjects) {
			    yyerror("Object registers already initialized!");
			} else {
			    tmppart[npart]->robjects = (char *)alloc(n_olist);
			    (void) memcpy((genericptr_t)tmppart[npart]->robjects,
					  (genericptr_t)olist, n_olist);
			    tmppart[npart]->nrobjects = n_olist;
			}
		  ;
    break;}
case 91:
#line 710 "lev_comp.y"
{
			if (tmppart[npart]->nloc) {
			    yyerror("Location registers already initialized!");
			} else {
			    register int i;
			    tmppart[npart]->rloc_x = (char *) alloc(n_plist);
			    tmppart[npart]->rloc_y = (char *) alloc(n_plist);
			    for(i=0;i<n_plist;i++) {
				tmppart[npart]->rloc_x[i] = plist[i].x;
				tmppart[npart]->rloc_y[i] = plist[i].y;
			    }
			    tmppart[npart]->nloc = n_plist;
			}
		  ;
    break;}
case 92:
#line 725 "lev_comp.y"
{
			if (tmppart[npart]->nrmonst) {
			    yyerror("Monster registers already initialized!");
			} else {
			    tmppart[npart]->rmonst = (char *) alloc(n_mlist);
			    (void) memcpy((genericptr_t)tmppart[npart]->rmonst,
					  (genericptr_t)mlist, n_mlist);
			    tmppart[npart]->nrmonst = n_mlist;
			}
		  ;
    break;}
case 93:
#line 738 "lev_comp.y"
{
			if (n_olist < MAX_REGISTERS)
			    olist[n_olist++] = yyvsp[0].i;
			else
			    yyerror("Object list too long!");
		  ;
    break;}
case 94:
#line 745 "lev_comp.y"
{
			if (n_olist < MAX_REGISTERS)
			    olist[n_olist++] = yyvsp[-2].i;
			else
			    yyerror("Object list too long!");
		  ;
    break;}
case 95:
#line 754 "lev_comp.y"
{
			if (n_mlist < MAX_REGISTERS)
			    mlist[n_mlist++] = yyvsp[0].i;
			else
			    yyerror("Monster list too long!");
		  ;
    break;}
case 96:
#line 761 "lev_comp.y"
{
			if (n_mlist < MAX_REGISTERS)
			    mlist[n_mlist++] = yyvsp[-2].i;
			else
			    yyerror("Monster list too long!");
		  ;
    break;}
case 97:
#line 770 "lev_comp.y"
{
			if (n_plist < MAX_REGISTERS)
			    plist[n_plist++] = current_coord;
			else
			    yyerror("Location list too long!");
		  ;
    break;}
case 98:
#line 777 "lev_comp.y"
{
			if (n_plist < MAX_REGISTERS)
			    plist[n_plist++] = current_coord;
			else
			    yyerror("Location list too long!");
		  ;
    break;}
case 122:
#line 813 "lev_comp.y"
{
			tmpmonst[nmons] = New(monster);
			tmpmonst[nmons]->x = current_coord.x;
			tmpmonst[nmons]->y = current_coord.y;
			tmpmonst[nmons]->class = yyvsp[-4].i;
			tmpmonst[nmons]->peaceful = -1; /* no override */
			tmpmonst[nmons]->asleep = -1;
			tmpmonst[nmons]->align = - MAX_REGISTERS - 2;
			tmpmonst[nmons]->name.str = 0;
			tmpmonst[nmons]->appear = 0;
			tmpmonst[nmons]->appear_as.str = 0;
			tmpmonst[nmons]->chance = yyvsp[-6].i;
			tmpmonst[nmons]->id = NON_PM;
			if (!in_room)
			    check_coord(current_coord.x, current_coord.y,
					"Monster");
			if (yyvsp[-2].map) {
			    int token = get_monster_id(yyvsp[-2].map, (char) yyvsp[-4].i);
			    if (token == ERR)
				yywarning(
			      "Invalid monster name!  Making random monster.");
			    else
				tmpmonst[nmons]->id = token;
			    Free(yyvsp[-2].map);
			}
		  ;
    break;}
case 123:
#line 840 "lev_comp.y"
{
			if (++nmons >= MAX_OF_TYPE) {
			    yyerror("Too many monsters in room or mazepart!");
			    nmons--;
			}
		  ;
    break;}
case 126:
#line 853 "lev_comp.y"
{
			tmpmonst[nmons]->name.str = yyvsp[0].map;
		  ;
    break;}
case 127:
#line 857 "lev_comp.y"
{
			tmpmonst[nmons]->peaceful = yyvsp[0].i;
		  ;
    break;}
case 128:
#line 861 "lev_comp.y"
{
			tmpmonst[nmons]->asleep = yyvsp[0].i;
		  ;
    break;}
case 129:
#line 865 "lev_comp.y"
{
			tmpmonst[nmons]->align = yyvsp[0].i;
		  ;
    break;}
case 130:
#line 869 "lev_comp.y"
{
			tmpmonst[nmons]->appear = yyvsp[-1].i;
			tmpmonst[nmons]->appear_as.str = yyvsp[0].map;
		  ;
    break;}
case 131:
#line 876 "lev_comp.y"
{
		  ;
    break;}
case 132:
#line 879 "lev_comp.y"
{
			/* 1: is contents of next object with 2 */
			/* 2: is a container */
			/* 0: neither */
			tmpobj[nobj-1]->containment = 2;
		  ;
    break;}
case 133:
#line 888 "lev_comp.y"
{
			tmpobj[nobj] = New(object);
			tmpobj[nobj]->class = yyvsp[-2].i;
			tmpobj[nobj]->corpsenm = NON_PM;
			tmpobj[nobj]->curse_state = -1;
			tmpobj[nobj]->name.str = 0;
			tmpobj[nobj]->chance = yyvsp[-4].i;
			tmpobj[nobj]->id = -1;
			if (yyvsp[0].map) {
			    int token = get_object_id(yyvsp[0].map);
			    if (token == ERR)
				yywarning(
				"Illegal object name!  Making random object.");
			     else
				tmpobj[nobj]->id = token;
			    Free(yyvsp[0].map);
			}
		  ;
    break;}
case 134:
#line 907 "lev_comp.y"
{
			if (++nobj >= MAX_OF_TYPE) {
			    yyerror("Too many objects in room or mazepart!");
			    nobj--;
			}
		  ;
    break;}
case 135:
#line 916 "lev_comp.y"
{
			tmpobj[nobj]->containment = 0;
			tmpobj[nobj]->x = current_coord.x;
			tmpobj[nobj]->y = current_coord.y;
			if (!in_room)
			    check_coord(current_coord.x, current_coord.y,
					"Object");
		  ;
    break;}
case 136:
#line 925 "lev_comp.y"
{
			tmpobj[nobj]->containment = 1;
			/* random coordinate, will be overridden anyway */
			tmpobj[nobj]->x = -MAX_REGISTERS-1;
			tmpobj[nobj]->y = -MAX_REGISTERS-1;
		  ;
    break;}
case 137:
#line 934 "lev_comp.y"
{
			tmpobj[nobj]->spe = -127;
	/* Note below: we're trying to make as many of these optional as
	 * possible.  We clearly can't make curse_state, enchantment, and
	 * monster_id _all_ optional, since ",random" would be ambiguous.
	 * We can't even just make enchantment mandatory, since if we do that
	 * alone, ",random" requires too much lookahead to parse.
	 */
		  ;
    break;}
case 138:
#line 944 "lev_comp.y"
{
		  ;
    break;}
case 139:
#line 947 "lev_comp.y"
{
		  ;
    break;}
case 140:
#line 950 "lev_comp.y"
{
		  ;
    break;}
case 141:
#line 955 "lev_comp.y"
{
			tmpobj[nobj]->curse_state = -1;
		  ;
    break;}
case 142:
#line 959 "lev_comp.y"
{
			tmpobj[nobj]->curse_state = yyvsp[0].i;
		  ;
    break;}
case 143:
#line 965 "lev_comp.y"
{
			int token = get_monster_id(yyvsp[0].map, (char)0);
			if (token == ERR)	/* "random" */
			    tmpobj[nobj]->corpsenm = NON_PM - 1;
			else
			    tmpobj[nobj]->corpsenm = token;
			Free(yyvsp[0].map);
		  ;
    break;}
case 144:
#line 976 "lev_comp.y"
{
			tmpobj[nobj]->spe = -127;
		  ;
    break;}
case 145:
#line 980 "lev_comp.y"
{
			tmpobj[nobj]->spe = yyvsp[0].i;
		  ;
    break;}
case 147:
#line 987 "lev_comp.y"
{
		  ;
    break;}
case 148:
#line 990 "lev_comp.y"
{
			tmpobj[nobj]->name.str = yyvsp[0].map;
		  ;
    break;}
case 149:
#line 996 "lev_comp.y"
{
			tmpdoor[ndoor] = New(door);
			tmpdoor[ndoor]->x = current_coord.x;
			tmpdoor[ndoor]->y = current_coord.y;
			tmpdoor[ndoor]->mask = yyvsp[-2].i;
			if(current_coord.x >= 0 && current_coord.y >= 0 &&
			   tmpmap[current_coord.y][current_coord.x] != DOOR &&
			   tmpmap[current_coord.y][current_coord.x] != SDOOR)
			    yyerror("Door decl doesn't match the map");
			ndoor++;
			if (ndoor >= MAX_OF_TYPE) {
				yyerror("Too many doors in mazepart!");
				ndoor--;
			}
		  ;
    break;}
case 150:
#line 1014 "lev_comp.y"
{
			tmptrap[ntrap] = New(trap);
			tmptrap[ntrap]->x = current_coord.x;
			tmptrap[ntrap]->y = current_coord.y;
			tmptrap[ntrap]->type = yyvsp[-2].i;
			tmptrap[ntrap]->chance = yyvsp[-4].i;
			if (!in_room)
			    check_coord(current_coord.x, current_coord.y,
					"Trap");
			if (++ntrap >= MAX_OF_TYPE) {
				yyerror("Too many traps in room or mazepart!");
				ntrap--;
			}
		  ;
    break;}
case 151:
#line 1031 "lev_comp.y"
{
		        int x, y, dir;

			tmpdb[ndb] = New(drawbridge);
			x = tmpdb[ndb]->x = current_coord.x;
			y = tmpdb[ndb]->y = current_coord.y;
			/* convert dir from a DIRECTION to a DB_DIR */
			dir = yyvsp[-2].i;
			switch(dir) {
			case W_NORTH: dir = DB_NORTH; y--; break;
			case W_SOUTH: dir = DB_SOUTH; y++; break;
			case W_EAST:  dir = DB_EAST;  x++; break;
			case W_WEST:  dir = DB_WEST;  x--; break;
			default:
			    yyerror("Invalid drawbridge direction");
			    break;
			}
			tmpdb[ndb]->dir = dir;
			if (current_coord.x >= 0 && current_coord.y >= 0 &&
			    !IS_WALL(tmpmap[y][x])) {
			    char ebuf[60];
			    Sprintf(ebuf,
				    "Wall needed for drawbridge (%02d, %02d)",
				    current_coord.x, current_coord.y);
			    yyerror(ebuf);
			}

			if ( yyvsp[0].i == D_ISOPEN )
			    tmpdb[ndb]->db_open = 1;
			else if ( yyvsp[0].i == D_CLOSED )
			    tmpdb[ndb]->db_open = 0;
			else
			    yyerror("A drawbridge can only be open or closed!");
			ndb++;
			if (ndb >= MAX_OF_TYPE) {
				yyerror("Too many drawbridges in mazepart!");
				ndb--;
			}
		   ;
    break;}
case 152:
#line 1073 "lev_comp.y"
{
			tmpwalk[nwalk] = New(walk);
			tmpwalk[nwalk]->x = current_coord.x;
			tmpwalk[nwalk]->y = current_coord.y;
			tmpwalk[nwalk]->dir = yyvsp[0].i;
			nwalk++;
			if (nwalk >= MAX_OF_TYPE) {
				yyerror("Too many mazewalks in mazepart!");
				nwalk--;
			}
		  ;
    break;}
case 153:
#line 1087 "lev_comp.y"
{
			wallify_map();
		  ;
    break;}
case 154:
#line 1093 "lev_comp.y"
{
			tmplad[nlad] = New(lad);
			tmplad[nlad]->x = current_coord.x;
			tmplad[nlad]->y = current_coord.y;
			tmplad[nlad]->up = yyvsp[0].i;
			if (!in_room)
			    check_coord(current_coord.x, current_coord.y,
					"Ladder");
			nlad++;
			if (nlad >= MAX_OF_TYPE) {
				yyerror("Too many ladders in mazepart!");
				nlad--;
			}
		  ;
    break;}
case 155:
#line 1110 "lev_comp.y"
{
			tmpstair[nstair] = New(stair);
			tmpstair[nstair]->x = current_coord.x;
			tmpstair[nstair]->y = current_coord.y;
			tmpstair[nstair]->up = yyvsp[0].i;
			if (!in_room)
			    check_coord(current_coord.x, current_coord.y,
					"Stairway");
			nstair++;
			if (nstair >= MAX_OF_TYPE) {
				yyerror("Too many stairs in room or mazepart!");
				nstair--;
			}
		  ;
    break;}
case 156:
#line 1127 "lev_comp.y"
{
			tmplreg[nlreg] = New(lev_region);
			tmplreg[nlreg]->in_islev = yyvsp[0].i;
			tmplreg[nlreg]->inarea.x1 = current_region.x1;
			tmplreg[nlreg]->inarea.y1 = current_region.y1;
			tmplreg[nlreg]->inarea.x2 = current_region.x2;
			tmplreg[nlreg]->inarea.y2 = current_region.y2;
		  ;
    break;}
case 157:
#line 1136 "lev_comp.y"
{
			tmplreg[nlreg]->del_islev = yyvsp[-2].i;
			tmplreg[nlreg]->delarea.x1 = current_region.x1;
			tmplreg[nlreg]->delarea.y1 = current_region.y1;
			tmplreg[nlreg]->delarea.x2 = current_region.x2;
			tmplreg[nlreg]->delarea.y2 = current_region.y2;
			if(yyvsp[0].i)
			    tmplreg[nlreg]->rtype = LR_UPSTAIR;
			else
			    tmplreg[nlreg]->rtype = LR_DOWNSTAIR;
			tmplreg[nlreg]->rname.str = 0;
			nlreg++;
			if (nlreg >= MAX_OF_TYPE) {
				yyerror("Too many levregions in mazepart!");
				nlreg--;
			}
		  ;
    break;}
case 158:
#line 1156 "lev_comp.y"
{
			tmplreg[nlreg] = New(lev_region);
			tmplreg[nlreg]->in_islev = yyvsp[0].i;
			tmplreg[nlreg]->inarea.x1 = current_region.x1;
			tmplreg[nlreg]->inarea.y1 = current_region.y1;
			tmplreg[nlreg]->inarea.x2 = current_region.x2;
			tmplreg[nlreg]->inarea.y2 = current_region.y2;
		  ;
    break;}
case 159:
#line 1165 "lev_comp.y"
{
			tmplreg[nlreg]->del_islev = yyvsp[-2].i;
			tmplreg[nlreg]->delarea.x1 = current_region.x1;
			tmplreg[nlreg]->delarea.y1 = current_region.y1;
			tmplreg[nlreg]->delarea.x2 = current_region.x2;
			tmplreg[nlreg]->delarea.y2 = current_region.y2;
			tmplreg[nlreg]->rtype = LR_PORTAL;
			tmplreg[nlreg]->rname.str = yyvsp[0].map;
			nlreg++;
			if (nlreg >= MAX_OF_TYPE) {
				yyerror("Too many levregions in mazepart!");
				nlreg--;
			}
		  ;
    break;}
case 160:
#line 1182 "lev_comp.y"
{
			tmplreg[nlreg] = New(lev_region);
			tmplreg[nlreg]->in_islev = yyvsp[0].i;
			tmplreg[nlreg]->inarea.x1 = current_region.x1;
			tmplreg[nlreg]->inarea.y1 = current_region.y1;
			tmplreg[nlreg]->inarea.x2 = current_region.x2;
			tmplreg[nlreg]->inarea.y2 = current_region.y2;
		  ;
    break;}
case 161:
#line 1191 "lev_comp.y"
{
			tmplreg[nlreg]->del_islev = yyvsp[0].i;
			tmplreg[nlreg]->delarea.x1 = current_region.x1;
			tmplreg[nlreg]->delarea.y1 = current_region.y1;
			tmplreg[nlreg]->delarea.x2 = current_region.x2;
			tmplreg[nlreg]->delarea.y2 = current_region.y2;
		  ;
    break;}
case 162:
#line 1199 "lev_comp.y"
{
			switch(yyvsp[0].i) {
			case -1: tmplreg[nlreg]->rtype = LR_TELE; break;
			case 0: tmplreg[nlreg]->rtype = LR_DOWNTELE; break;
			case 1: tmplreg[nlreg]->rtype = LR_UPTELE; break;
			}
			tmplreg[nlreg]->rname.str = 0;
			nlreg++;
			if (nlreg >= MAX_OF_TYPE) {
				yyerror("Too many levregions in mazepart!");
				nlreg--;
			}
		  ;
    break;}
case 163:
#line 1215 "lev_comp.y"
{
			tmplreg[nlreg] = New(lev_region);
			tmplreg[nlreg]->in_islev = yyvsp[0].i;
			tmplreg[nlreg]->inarea.x1 = current_region.x1;
			tmplreg[nlreg]->inarea.y1 = current_region.y1;
			tmplreg[nlreg]->inarea.x2 = current_region.x2;
			tmplreg[nlreg]->inarea.y2 = current_region.y2;
		  ;
    break;}
case 164:
#line 1224 "lev_comp.y"
{
			tmplreg[nlreg]->del_islev = yyvsp[0].i;
			tmplreg[nlreg]->delarea.x1 = current_region.x1;
			tmplreg[nlreg]->delarea.y1 = current_region.y1;
			tmplreg[nlreg]->delarea.x2 = current_region.x2;
			tmplreg[nlreg]->delarea.y2 = current_region.y2;
			tmplreg[nlreg]->rtype = LR_BRANCH;
			tmplreg[nlreg]->rname.str = 0;
			nlreg++;
			if (nlreg >= MAX_OF_TYPE) {
				yyerror("Too many levregions in mazepart!");
				nlreg--;
			}
		  ;
    break;}
case 165:
#line 1241 "lev_comp.y"
{
			yyval.i = -1;
		  ;
    break;}
case 166:
#line 1245 "lev_comp.y"
{
			yyval.i = yyvsp[0].i;
		  ;
    break;}
case 167:
#line 1251 "lev_comp.y"
{
			yyval.i = 0;
		  ;
    break;}
case 168:
#line 1255 "lev_comp.y"
{
/* This series of if statements is a hack for MSC 5.1.  It seems that its
   tiny little brain cannot compile if these are all one big if statement. */
			if (yyvsp[-7].i <= 0 || yyvsp[-7].i >= COLNO)
				yyerror("Region out of level range!");
			else if (yyvsp[-5].i < 0 || yyvsp[-5].i >= ROWNO)
				yyerror("Region out of level range!");
			else if (yyvsp[-3].i <= 0 || yyvsp[-3].i >= COLNO)
				yyerror("Region out of level range!");
			else if (yyvsp[-1].i < 0 || yyvsp[-1].i >= ROWNO)
				yyerror("Region out of level range!");
			current_region.x1 = yyvsp[-7].i;
			current_region.y1 = yyvsp[-5].i;
			current_region.x2 = yyvsp[-3].i;
			current_region.y2 = yyvsp[-1].i;
			yyval.i = 1;
		  ;
    break;}
case 169:
#line 1275 "lev_comp.y"
{
			tmpfountain[nfountain] = New(fountain);
			tmpfountain[nfountain]->x = current_coord.x;
			tmpfountain[nfountain]->y = current_coord.y;
			if (!in_room)
			    check_coord(current_coord.x, current_coord.y,
					"Fountain");
			nfountain++;
			if (nfountain >= MAX_OF_TYPE) {
			    yyerror("Too many fountains in room or mazepart!");
			    nfountain--;
			}
		  ;
    break;}
case 170:
#line 1291 "lev_comp.y"
{
			tmpsink[nsink] = New(sink);
			tmpsink[nsink]->x = current_coord.x;
			tmpsink[nsink]->y = current_coord.y;
			nsink++;
			if (nsink >= MAX_OF_TYPE) {
				yyerror("Too many sinks in room!");
				nsink--;
			}
		  ;
    break;}
case 171:
#line 1304 "lev_comp.y"
{
			tmppool[npool] = New(pool);
			tmppool[npool]->x = current_coord.x;
			tmppool[npool]->y = current_coord.y;
			npool++;
			if (npool >= MAX_OF_TYPE) {
				yyerror("Too many pools in room!");
				npool--;
			}
		  ;
    break;}
case 172:
#line 1317 "lev_comp.y"
{
			tmpdig[ndig] = New(digpos);
			tmpdig[ndig]->x1 = current_region.x1;
			tmpdig[ndig]->y1 = current_region.y1;
			tmpdig[ndig]->x2 = current_region.x2;
			tmpdig[ndig]->y2 = current_region.y2;
			ndig++;
			if (ndig >= MAX_OF_TYPE) {
				yyerror("Too many diggables in mazepart!");
				ndig--;
			}
		  ;
    break;}
case 173:
#line 1332 "lev_comp.y"
{
			tmppass[npass] = New(digpos);
			tmppass[npass]->x1 = current_region.x1;
			tmppass[npass]->y1 = current_region.y1;
			tmppass[npass]->x2 = current_region.x2;
			tmppass[npass]->y2 = current_region.y2;
			npass++;
			if (npass >= 32) {
				yyerror("Too many passwalls in mazepart!");
				npass--;
			}
		  ;
    break;}
case 174:
#line 1347 "lev_comp.y"
{
			tmpreg[nreg] = New(region);
			tmpreg[nreg]->x1 = current_region.x1;
			tmpreg[nreg]->y1 = current_region.y1;
			tmpreg[nreg]->x2 = current_region.x2;
			tmpreg[nreg]->y2 = current_region.y2;
			tmpreg[nreg]->rlit = yyvsp[-3].i;
			tmpreg[nreg]->rtype = yyvsp[-1].i;
			if(yyvsp[0].i & 1) tmpreg[nreg]->rtype += MAXRTYPE+1;
			tmpreg[nreg]->rirreg = ((yyvsp[0].i & 2) != 0);
			if(current_region.x1 > current_region.x2 ||
			   current_region.y1 > current_region.y2)
			   yyerror("Region start > end!");
			if(tmpreg[nreg]->rtype == VAULT &&
			   (tmpreg[nreg]->rirreg ||
			    (tmpreg[nreg]->x2 - tmpreg[nreg]->x1 != 1) ||
			    (tmpreg[nreg]->y2 - tmpreg[nreg]->y1 != 1)))
				yyerror("Vaults must be exactly 2x2!");
			if(want_warnings && !tmpreg[nreg]->rirreg &&
			   current_region.x1 > 0 && current_region.y1 > 0 &&
			   current_region.x2 < (int)max_x_map &&
			   current_region.y2 < (int)max_y_map) {
			    /* check for walls in the room */
			    char ebuf[60];
			    register int x, y, nrock = 0;

			    for(y=current_region.y1; y<=current_region.y2; y++)
				for(x=current_region.x1;
				    x<=current_region.x2; x++)
				    if(IS_ROCK(tmpmap[y][x]) ||
				       IS_DOOR(tmpmap[y][x])) nrock++;
			    if(nrock) {
				Sprintf(ebuf,
					"Rock in room (%02d,%02d,%02d,%02d)?!",
					current_region.x1, current_region.y1,
					current_region.x2, current_region.y2);
				yywarning(ebuf);
			    }
			    if (
		!IS_ROCK(tmpmap[current_region.y1-1][current_region.x1-1]) ||
		!IS_ROCK(tmpmap[current_region.y2+1][current_region.x1-1]) ||
		!IS_ROCK(tmpmap[current_region.y1-1][current_region.x2+1]) ||
		!IS_ROCK(tmpmap[current_region.y2+1][current_region.x2+1])) {
				Sprintf(ebuf,
				"NonRock edge in room (%02d,%02d,%02d,%02d)?!",
					current_region.x1, current_region.y1,
					current_region.x2, current_region.y2);
				yywarning(ebuf);
			    }
			} else if(tmpreg[nreg]->rirreg &&
		!IS_ROOM(tmpmap[current_region.y1][current_region.x1])) {
			    char ebuf[60];
			    Sprintf(ebuf,
				    "Rock in irregular room (%02d,%02d)?!",
				    current_region.x1, current_region.y1);
			    yyerror(ebuf);
			}
			nreg++;
			if (nreg >= MAX_OF_TYPE) {
				yyerror("Too many regions in mazepart!");
				nreg--;
			}
		  ;
    break;}
case 175:
#line 1413 "lev_comp.y"
{
			tmpaltar[naltar] = New(altar);
			tmpaltar[naltar]->x = current_coord.x;
			tmpaltar[naltar]->y = current_coord.y;
			tmpaltar[naltar]->align = yyvsp[-2].i;
			tmpaltar[naltar]->shrine = yyvsp[0].i;
			if (!in_room)
			    check_coord(current_coord.x, current_coord.y,
					"Altar");
			naltar++;
			if (naltar >= MAX_OF_TYPE) {
				yyerror("Too many altars in room or mazepart!");
				naltar--;
			}
		  ;
    break;}
case 176:
#line 1431 "lev_comp.y"
{
			tmpgold[ngold] = New(gold);
			tmpgold[ngold]->x = current_coord.x;
			tmpgold[ngold]->y = current_coord.y;
			tmpgold[ngold]->amount = yyvsp[-2].i;
			if (!in_room)
			    check_coord(current_coord.x, current_coord.y,
					"Gold");
			ngold++;
			if (ngold >= MAX_OF_TYPE) {
				yyerror("Too many golds in room or mazepart!");
				ngold--;
			}
		  ;
    break;}
case 177:
#line 1448 "lev_comp.y"
{
			tmpengraving[nengraving] = New(engraving);
			tmpengraving[nengraving]->x = current_coord.x;
			tmpengraving[nengraving]->y = current_coord.y;
			tmpengraving[nengraving]->engr.str = yyvsp[0].map;
			tmpengraving[nengraving]->etype = yyvsp[-2].i;
			if (!in_room)
			    check_coord(current_coord.x, current_coord.y,
					"Engraving");
			nengraving++;
			if (nengraving >= MAX_OF_TYPE) {
			    yyerror("Too many engravings in room or mazepart!");
			    nengraving--;
			}
		  ;
    break;}
case 179:
#line 1467 "lev_comp.y"
{
			yyval.i = - MAX_REGISTERS - 1;
		  ;
    break;}
case 182:
#line 1475 "lev_comp.y"
{
			yyval.i = - MAX_REGISTERS - 1;
		  ;
    break;}
case 185:
#line 1483 "lev_comp.y"
{
			yyval.map = (char *) 0;
		  ;
    break;}
case 187:
#line 1490 "lev_comp.y"
{
			yyval.map = (char *) 0;
		  ;
    break;}
case 188:
#line 1496 "lev_comp.y"
{
			int token = get_trap_type(yyvsp[0].map);
			if (token == ERR)
				yyerror("Unknown trap type!");
			yyval.i = token;
			Free(yyvsp[0].map);
		  ;
    break;}
case 190:
#line 1507 "lev_comp.y"
{
			int token = get_room_type(yyvsp[0].map);
			if (token == ERR) {
				yywarning("Unknown room type!  Making ordinary room...");
				yyval.i = OROOM;
			} else
				yyval.i = token;
			Free(yyvsp[0].map);
		  ;
    break;}
case 192:
#line 1520 "lev_comp.y"
{
			yyval.i = 0;
		  ;
    break;}
case 193:
#line 1524 "lev_comp.y"
{
			yyval.i = yyvsp[0].i;
		  ;
    break;}
case 194:
#line 1528 "lev_comp.y"
{
			yyval.i = yyvsp[-2].i + (yyvsp[0].i << 1);
		  ;
    break;}
case 197:
#line 1536 "lev_comp.y"
{
			current_coord.x = current_coord.y = -MAX_REGISTERS-1;
		  ;
    break;}
case 204:
#line 1552 "lev_comp.y"
{
			yyval.i = - MAX_REGISTERS - 1;
		  ;
    break;}
case 207:
#line 1562 "lev_comp.y"
{
			if ( yyvsp[-1].i >= MAX_REGISTERS )
				yyerror("Register Index overflow!");
			else
				current_coord.x = current_coord.y = - yyvsp[-1].i - 1;
		  ;
    break;}
case 208:
#line 1571 "lev_comp.y"
{
			if ( yyvsp[-1].i >= MAX_REGISTERS )
				yyerror("Register Index overflow!");
			else
				yyval.i = - yyvsp[-1].i - 1;
		  ;
    break;}
case 209:
#line 1580 "lev_comp.y"
{
			if ( yyvsp[-1].i >= MAX_REGISTERS )
				yyerror("Register Index overflow!");
			else
				yyval.i = - yyvsp[-1].i - 1;
		  ;
    break;}
case 210:
#line 1589 "lev_comp.y"
{
			if ( yyvsp[-1].i >= 3 )
				yyerror("Register Index overflow!");
			else
				yyval.i = - yyvsp[-1].i - 1;
		  ;
    break;}
case 212:
#line 1601 "lev_comp.y"
{
			if (check_monster_char((char) yyvsp[0].i))
				yyval.i = yyvsp[0].i ;
			else {
				yyerror("Unknown monster class!");
				yyval.i = ERR;
			}
		  ;
    break;}
case 213:
#line 1612 "lev_comp.y"
{
			char c = yyvsp[0].i;
			if (check_object_char(c))
				yyval.i = c;
			else {
				yyerror("Unknown char class!");
				yyval.i = ERR;
			}
		  ;
    break;}
case 217:
#line 1631 "lev_comp.y"
{
			yyval.i = 100;	/* default is 100% */
		  ;
    break;}
case 218:
#line 1635 "lev_comp.y"
{
			if (yyvsp[0].i <= 0 || yyvsp[0].i > 100)
			    yyerror("Expected percentile chance.");
			yyval.i = yyvsp[0].i;
		  ;
    break;}
case 221:
#line 1647 "lev_comp.y"
{
			if (!in_room && !init_lev.init_present &&
			    (yyvsp[-3].i < 0 || yyvsp[-3].i > (int)max_x_map ||
			     yyvsp[-1].i < 0 || yyvsp[-1].i > (int)max_y_map))
			    yyerror("Coordinates out of map range!");
			current_coord.x = yyvsp[-3].i;
			current_coord.y = yyvsp[-1].i;
		  ;
    break;}
case 222:
#line 1658 "lev_comp.y"
{
/* This series of if statements is a hack for MSC 5.1.  It seems that its
   tiny little brain cannot compile if these are all one big if statement. */
			if (yyvsp[-7].i < 0 || yyvsp[-7].i > (int)max_x_map)
				yyerror("Region out of map range!");
			else if (yyvsp[-5].i < 0 || yyvsp[-5].i > (int)max_y_map)
				yyerror("Region out of map range!");
			else if (yyvsp[-3].i < 0 || yyvsp[-3].i > (int)max_x_map)
				yyerror("Region out of map range!");
			else if (yyvsp[-1].i < 0 || yyvsp[-1].i > (int)max_y_map)
				yyerror("Region out of map range!");
			current_region.x1 = yyvsp[-7].i;
			current_region.y1 = yyvsp[-5].i;
			current_region.x2 = yyvsp[-3].i;
			current_region.y2 = yyvsp[-1].i;
		  ;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 498 "/usr/lib/bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;
}
#line 1676 "lev_comp.y"


/*lev_comp.y*/
