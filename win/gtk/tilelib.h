/*
 * Nethack tiletext format library -- intended for a plugin for the GIMP
 * but can be used anywhere
 *
 * Much of this code is borrowed from win/share/tile.h in the standard 
 * Nethack distribution.
 *
 * This code is covered by the NetHack Public License.
 *
 * Hacked by Rockwalrus (Nathan Summers)
 */

#include <stdio.h>
#define SIZE(x) (int)(sizeof(x)/sizeof(x[0]))

#define MAXCOLORMAPSIZE 	256

#define CM_RED		0
#define CM_GREEN	1
#define CM_BLUE 	2

typedef int boolean;
typedef unsigned char pixval;

typedef struct {
    pixval r, g, b;
} pixel;

typedef struct {
	char *name;
	pixel **data;
} tile;

typedef struct {
	pixval d[3][MAXCOLORMAPSIZE];
	
	int n;
} colormap;

typedef struct {
	short d[MAXCOLORMAPSIZE];
	int n;
} indexmap;

typedef struct {
	FILE *file;
	char *name;
	colormap cmap;
	indexmap imap;
	int xsize, ysize;
} tiletext_file;
	

#define Fprintf (void) fprintf

extern void tiletext_read_colormap(tiletext_file *);
extern boolean tiletext_write_colormap(tiletext_file *);
extern boolean tiletext_read(tiletext_file *, tile *);
extern void tiletext_write(tiletext_file *, tile *);
void tiletext_read_size(tiletext_file *);
extern void copy_colormap(colormap *, colormap *);
extern voidmerge_colormap(colormap *, colormap *);
extern tiletext_file *tiletext_open(const char *, const char *);
#if defined(MICRO)
#undef exit
# if !defined(MSDOS) && !defined(WIN32)
extern void exit (int);
# endif
#endif
