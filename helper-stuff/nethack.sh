#!/bin/sh

set -e

if [ "x$NETHACKOPTIONS" == x ];
then
    if [ ! -f ~/.nethackrc ] ; then
            echo \
    "warning: no .nethackrc file in home dir or NETHACKOPTIONS env var"
        if [ -f /etc/nethackrc ] ; then
            echo "copying default /etc/nethackrc to your home directory."
            echo "you are encouraged to edit it to your liking."
            cp /etc/nethackrc ~/.nethackrc
            NETHACKOPTIONS=~/.nethackrc
            export NETHACKOPTIONS
        else
            echo "warning: no default /etc/nethackrc file found..."
        fi;
    else
        NETHACKOPTIONS=~/.nethackrc
        export NETHACKOPTIONS
    fi;
fi;
exec /usr/lib/games/gnomehack/gnomehack.sh $*
