#!/bin/sh

# Nethack save-file recovery script for Debian
#
# Ben Gertzfield (che@debian.org) 29 July 1997
# Copyright 1997 Ben Gertzfield. This script is released under the
# GNU General Public License, version 2 or later.
#
# chkconfig: 2345 85 85
# description: Nethack save-game file recovery script \
#              ensures that you never lose a game interrupted by \
#              a power failure or other nasty things.

PATH=/bin:/usr/bin:/sbin:/usr/sbin

set -e

case "$1" in
    start)
	# Has the gnomehack package been removed?
	test -f /usr/lib/games/gnomehack/recover_gnomehack || exit 0

	# Are there any lock-files to recover?
	ls /var/lib/games/gnomehack/*lock.0 >/dev/null 2>&1 || exit 0

	# Yes; recover them.
	for file in /var/lib/games/gnomehack/*lock.0; do
	    owner=$(ls -l $file | awk '{print $3}')
	    echo -n "Recovering GnomeHack save file owned by $owner: "
	    su $owner -c "/usr/lib/games/gnomehack/recover_gnomehack $file" >/dev/null 2>&1
	    if (($?)); then
	      echo error $?\!
	    else
	      echo "recovered."
	    fi
	done


    ;;

    stop)
    ;;

    restart)
    ;;
    
    force-reload)
    ;;

    reload)
    ;;

    *)
	echo "Usage: /etc/init.d/gnomehack.init.sh {start|stop|restart|reload|force-reload}"
	exit 1
    ;;
esac

exit 0
