#ifndef __NHALLOC_H__
#define __NHALLOC_H__

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifndef __cplusplus  /* "alloc" is already defined in c++ */
E long *FDECL(alloc, (unsigned int));
#endif /* __cplusplus */

#endif /*  __NHALLOC_H__  */
