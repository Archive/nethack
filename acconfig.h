#ifndef __NH_CONFIG_H__
#define __NH_CONFIG_H__

@TOP@
@BOTTOM@

/* some things autoheader complained about */
#undef HAVE_LIBSM

/* Define if NetHack conflicts w/ system <curses.h> header file.  */
#undef HAVE_CURSES_H_CONFLICT

/* Define if you have a stupid compiler */
#undef STUPID

/* Define to the appropriate generic pointer */
#undef genericptr

/* Define if your compiler chokes on genericptr_t */
#undef genericptr_t

/* Define if your compiler chokes on time_t */
#undef time_t

/* Define if your compiler chokes on volatile */
#undef volatile

/* define if you are running on unix */
#undef UNIX

/* define if your C compiler chokes on some complicated expressions */
#undef STUPID

/* prototype support */
#undef PROTOTYPES
#undef UNWIDENED_PROTOTYPES
#undef WIDENED_PROTOTYPES

/* define if compiler chokes on some kinds of prototype */
#undef ULTRIX_PROTO	

/* define if defined() does not work properly */
#undef KR1ED

/* define to a high-quality random-number generator like random or lrand48 */
#undef RAND_FUNC

/* define if you don't have a high-quality random-number generator */
#undef RANDOM

/* define if you have usleep */
#undef HAVE_USLEEP

/* define if you have napms */
#undef HAVE_NAPMS

/* some versions of gcc seriously muck up nested loops. if you get strange
   crashes while searching in a version compiled with gcc, try putting
   #define GCC_BUG in *conf.h (or adding -DGCC_BUG to CFLAGS in the
   makefile).  */
/* NOTE: *not* defined in configure.in, because only the TOS port
   appears to need this.  Only included for completeness */
#undef GCC_BUG

/* define if code to workaround GCC warnings is desired */
#undef GCC_WARN

/* Define autoconf var if we want to #include any X11 pixmaps */
#undef XPM_LIST

/* Define to compile tiles into NetHack binary */
#undef INTERNAL_TILES

/* Define to DISable signal handling */
#undef NO_SIGNAL

/* System checks for randomize functions */
#undef HAVE_SRAND48
#undef HAVE_SRANDOM


/*
 * header files to include before anything else
 */
#ifdef HAVE_STRING_H
#include <string.h>
#endif

/*
 * Windowing systems...
 */

/* good old tty based graphics */
#undef TTY_GRAPHICS

/* X11 interface */
#undef X11_GRAPHICS

/* Gtk+/GNOME interface */
#undef GNOME_GRAPHICS
#undef HAVE_ORBIT

/* Enable use of tiles instead of characters to represent game objs */
#undef USE_TILES


/*
 * Define the default window system.  This should be one that is compiled
 * into your system (see defines above).  Known window systems are:
 *
 *	tty, X11, gnome
 */
#undef DEFAULT_WINDOW_SYS

/* uses terminfo rather than termcap */
#define TERMINFO

/* define if a terminal handles highlighting or tabs poorly */
#undef MINIMAL_TERM


/*
 * There are two ways that X11 tiles may be defined.  (1) using a custom
 * format loaded by NetHack code, or (2) using the XPM format loaded by
 * the free XPM library.  The second option allows you to then use other
 * programs to generate tiles files.  For example, the PBMPlus tools
 * would allow:
 *  xpmtoppm <x11tiles.xpm | pnmscale 1.25 | ppmquant 90 >x11tiles_big.xpm
 */
 
/* Define if you wish to use xpm's */
#undef USE_XPM
#undef USE_BIGTILES
#undef RADAR

/* Define to use a graphical tombstone (rip.xpm) */
#undef GRAPHIC_TOMBSTONE

/*
 * Section 2:	Some global parameters and filenames.
 *		Commenting out WIZARD, LOGFILE, or NEWS removes that feature
 *		from the game; otherwise set the appropriate wizard name.
 *		LOGFILE and NEWS refer to files in the playground.
 */

/* Define to enable debugging output */
#undef DEBUG

/* 
 * Define as the person allowed to use the -D option  (wizard mode, debugging and 
 * cheating, ha ha) 
 */
#undef WIZARD

/* 
 * Enable LZA/WWA's awesome Wizard patch '96
 */
#undef FEATURE_WIZ_PATCH


/*
 * if your preprocessor can't handle commands in the form #if defined(BLAH), 
 * instead of define WIZARD as the username, do a #define WIZARD and set this
 * to the username.
 *
 * Otherwise, do not define this.
 */
#undef WIZARD_NAME

/*
 *	If COMPRESS is defined, it should contain the full path name of your
 *	'compress' program.  Defining INTERNAL_COMP causes NetHack to do
 *	simpler byte-stream compression internally.  Both COMPRESS and
 *	INTERNAL_COMP create smaller bones/level/save files, but require
 *	additional code and time.  Currently, only UNIX fully implements
 *	COMPRESS; other ports should be able to uncompress save files a
 *	la unixmain.c if so inclined.
 *	If you define COMPRESS, you must also define COMPRESS_EXTENSION
 *	as the extension your compressor appends to filenames after
 *	compression.
 */


/* define to a compression program that works like compress or gzip */
#undef COMPRESS

/* define to the normal extension for that program */
#undef COMPRESS_EXTENSION

/* 
 *define to use nethack's internal compression, I guess.  It's not actually used
 * in the code as far as I can tell
 */
#undef INTERNAL_COMP	

/*
 *	Data librarian.  Defining DLB places most of the support files into
 *	a tar-like file, thus making a neater installation.  See *conf.h
 *	for detailed configuration.
 */
#undef DLB

/*
 *	Defining INSURANCE slows down level changes, but allows games that
 *	died due to program or system crashes to be resumed from the point
 *	of the last level change, after running a utility program.
 */
#define INSURANCE	/* allow crashed game recovery */


#ifndef HAVE_CHDIR
#  error chdir() required
#else
#  define CHDIR
#endif

#ifdef CHDIR
/*
 * If you define HACKDIR, then this will be the default playground;
 * otherwise it will be the current directory.
 */
/* NOTE: HACKDIR now defined in Makefile.am */


/*
 *  * If you define SAVEGAMEDIR, then this will be the save game directory
 *   */
/* NOTE: SAVEGAMEDIR is now defined in Makefile.am */
#undef LOGFILE  /* "logfile" */  /* larger file for debugging purposes */
#undef NEWS  /* "news"     */    /* the file containing the latest hack news */
#undef HLOCK  /* "perm"    */    /* an empty file used for locking purposes */
#undef RECORD  /* "record" */


/*
 * Some system administrators are stupid enough to make Hack suid root
 * or suid daemon, where daemon has other powers besides that of reading or
 * writing Hack files.  In such cases one should be careful with chdir's
 * since the user might create files in a directory of his choice.
 * Of course SECURE is meaningful only if HACKDIR is defined.
 */
 
/* define to do setuid(getuid()) after chdir() */
#undef SECURE	

/*
 * If it is desirable to limit the number of people that can play Hack
 * simultaneously, define HACKDIR, SECURE, and set MAX_NR_OF_PLAYERS to the 
 * maximum number of players.
 */

#undef MAX_NR_OF_PLAYERS
  
#endif /* CHDIR */



/*
 * Section 3:	Definitions that may vary with system type.
 *		For example, both schar and uchar should be short ints on
 *		the AT&T 3B2/3B5/etc. family.
 */

#undef SCHAR

#undef UCHAR


/*
 * type schar: small signed integers (8 bits suffice) (eg. TOS) */

typedef SCHAR	schar;

/*
 * type uchar: small unsigned integers (8 bits suffice - but 7 bits do not)
 *
 *	typedef unsigned char	uchar;
 *
 *	will be satisfactory if you have an "unsigned char" type;
 *	otherwise use
 *
 *	typedef unsigned short int uchar;
 */
typedef UCHAR	uchar;

/*
 * Various structures have the option of using bitfields to save space.
 * If your C compiler handles bitfields well (e.g., it can initialize structs
 * containing bitfields), you can define BITFIELDS.  Otherwise, the game will
 * allocate a separate character for each bitfield.  (The bitfields used never
 * have more than 7 bits, and most are only 1 bit.)
 */

#undef BITFIELDS

/* handle various names for case-insensitive string comparison */
#if defined(HAVE_STRNCMPI) || defined(HAVE_STRNCASECMP)
#  define STRNCMPI 
#  if defined(HAVE_STRNCASECMP) && !defined(HAVE_STRNCMPI)
#    define strncmpi(s1, s2, n) strncasecmp(s1, s2, n)
#  endif
#endif

#if defined(HAVE_STRCASECMP) && !defined(HAVE_STRCMPI)
#  define strcmpi(s1, s2) strcasecmp(s1, s2)
#endif

/*
 * There are various choices for the NetHack vision system.  There is a
 * choice of two algorithms with the same behavior.  Defining VISION_TABLES
 * creates huge (60K) tables at compile time, drastically increasing data
 * size, but runs slightly faster than the alternate algorithm.  (MSDOS in
 * particular cannot tolerate the increase in data size; other systems can
 * flip a coin weighted to local conditions.)
 *
 * If VISION_TABLES is not defined, things will be faster if you can use
 * MACRO_CPATH.  Some cpps, however, cannot deal with the size of the
 * functions that have been macroized.
 */

/* define only one of these three */

/* define to use vision tables generated at compile time */
#undef VISION_TABLES

/* use clear_path macros instead of functions */
#undef MACRO_CPATH	

/* use clear_path functions instead of macros  */
#undef NO_MACRO_CPATH	

/*
 * Section 4:  THE FUN STUFF!!!
 *
 * Conditional compilation of special options are controlled here.
 * If you define the following flags, you will add not only to the
 * complexity of the game but also to the size of the load module.
 */

/*
 * dungeon features
 */
/* Weapon skills - Stephen White */
#undef WEAPON_SKILLS
/* Kitchen sinks - Janet Walz */
#undef SINKS

/*
 * dungeon levels
 */
/* Fancy mazes - Jean-Christophe Collet */
#undef WALLIFIED_MAZE
/* Special Rogue-like levels */
#undef REINCARNATION

/*
 * monsters & objects
 */
/* Keystone Kops by Scott R. Turner */
#undef KOPS
/* Succubi/incubi seduction, by KAA, suggested by IM */
#undef SEDUCE
/* Tourist players with cameras and Hawaiian shirts */
#undef TOURIST

/* difficulty */
/* Engraving the E-word repels monsters */
#undef ELBERETH

/* I/O */
/* support for redoing last command - DGK */
#undef REDO

/* allow smaller screens -- ERS */
#undef CLIPPING


#ifdef REDO
# define DOAGAIN '\001'	/* ^A, the "redo" key used in cmd.c and getline.c */
#endif

/* Show experience on bottom line */
#undef EXP_ON_BOTL
/* added by Gary Erickson (erickson@ucivax) */
#undef SCORE_ON_BOTL



/* if running on a networked system */
#undef NETWORK

/* Use System V r3.2 terminfo color support */
#define TEXTCOLOR

/* avoid a problem using OpenWindows 3.0 for X11
   on SunOS 4.1.x, x>= 2.  Do not define for other
   X11 implementations. */
/* #define OPENWINBUG */

/* avoid a bug on the Pyramid */
/* #define PYRAMID_BUG */

/* for real 4.3BSD cc's without schain botch fix */
/* #define BSD_43_BUG */

/* problems with large arrays in structs */
/* #define MICROPORT_BUG  */

/* Changes needed in termcap.c to get it to
   run with Microport Sys V/AT version 2.4.
   By Jay Maynard */
/* #define MICROPORT_286_BUG */

/* avoid a problem with little_to_big() optimization */
/* #define AIXPS_2BUG */

/* see sys/unix/snd86.shr for more information on these */
/* Play real music through speaker on systems with
   music driver installed */
/* #define UNIX386MUSIC */

/* Play real music through speaker on systems with
   built-in VPIX support */
/* #define VPIX_MUSIC */


#define Sprintf (void) sprintf
#define Strcat (void) strcat
#define Strcpy (void) strcpy
#define Vsprintf (void) vsprintf
#define Vprintf (void) vprintf
#define Vfprintf (void) vfprintf

#ifdef __GNUC__
# if __GNUC__ >= 2
#define PRINTF_F(f,v) __attribute__ ((format (printf, f, v)))
# endif
#endif
#ifndef PRINTF_F
#define PRINTF_F(f,v)
#endif

#ifndef genericptr_t
typedef void * genericptr_t;
#endif


#ifdef HAVE_USLEEP
# define msleep(k) usleep((k)*1000)
# define TIMED_DELAY
#else
# ifdef HAVE_NAPMS
#  define msleep(k) napms(k)
#  define TIMED_DELAY
# endif
#endif

#define NEARDATA

#ifdef TM_IN_SYS_TIME
#include <sys/time.h>
#else
#include <time.h>
#endif

#ifdef RAND_FUNC
#define Rand() RAND_FUNC ()
#endif

#ifdef MSLEEP_MACRO
#define msleep(k) MSLEEP_MACRO
#define TIMED_DELAY
#endif

#ifdef STDC_HEADERS
# define USE_STDARG
#include <stdarg.h>
# define VA_DECL(typ1,var1)     (typ1 var1, ...) { va_list the_args;
# define VA_DECL2(typ1,var1,typ2,var2)  \
        (typ1 var1, typ2 var2, ...) { va_list the_args;
# define VA_INIT(var1,typ1)
# define VA_NEXT(var1,typ1)     var1 = va_arg(the_args, typ1)
# define VA_ARGS                the_args
# define VA_START(x)            va_start(the_args, x)
# define VA_END()               va_end(the_args)
#else
# ifdef HAVE_VARARGS_H
#  define USE_VARARGS
#include <varargs.h>
#  define VA_DECL(typ1,var1)    (va_alist) va_dcl {\
                va_list the_args; typ1 var1;
#  define VA_DECL2(typ1,var1,typ2,var2) (va_alist) va_dcl {\
                va_list the_args; typ1 var1; typ2 var2;
#  define VA_ARGS               the_args
#  define VA_START(x)           va_start(the_args)
#  define VA_INIT(var1,typ1)    var1 = va_arg(the_args, typ1)
#  define VA_NEXT(var1,typ1)    var1 = va_arg(the_args,typ1)
#  define VA_END()              va_end(the_args)
# else
#  define USE_OLDARGS
#  define VA_ARGS      arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8,arg9
#  define VA_DECL(typ1,var1)  (var1,VA_ARGS) typ1 var1; \
        char *arg1,*arg2,*arg3,*arg4,*arg5,*arg6,*arg7,*arg8,*arg9; {
#  define VA_DECL2(typ1,var1,typ2,var2)  (var1,var2,VA_ARGS) \
        typ1 var1; typ2 var2;\
        char *arg1,*arg2,*arg3,*arg4,*arg5,*arg6,*arg7,*arg8,*arg9; {
#  define VA_START(x)
#  define VA_INIT(var1,typ1)
#  define VA_END()
# endif
#endif

#ifdef PROTOTYPES
# define NDECL(f)	f(void)
# define FDECL(f,p)	f p
# define VDECL(f,p)	f p
#else
# define NDECL(f)	f()
# define FDECL(f,p)	f()
# define VDECL(f,p)	f()
#endif

#ifdef KR1ED		/* For compilers which cannot handle defined() */
#define defined(x) (-x-1 != -1)
/* Because:
 * #define FOO => FOO={} => defined( ) => (-1 != - - 1) => 1
 * #define FOO 1 or on command-line -DFOO
 *      => defined(1) => (-1 != - 1 - 1) => 1
 * if FOO isn't defined, FOO=0. But some compilers default to 0 instead of 1
 * for -DFOO, oh well.
 *      => defined(0) => (-1 != - 0 - 1) => 0
 *
 * But:
 * defined("") => (-1 != - "" - 1)
 *   [which is an unavoidable catastrophe.]
 */
#endif



/*
 * The next two defines are intended mainly for the Andrew File System,
 * which does not allow hard links.  If NO_FILE_LINKS is defined, lock files
 * will be created in LOCKDIR using open() instead of in the playground using
 * link().
 *		Ralf Brown, 7/26/89 (from v2.3 hack of 10/10/88)
 */

/* if no hard links */
/* #define NO_FILE_LINKS */
#define LOCKDIR "/var/lib/games/nethack"	/* where to put locks */


/*
 * Define DEF_PAGER as your default pager, e.g. "/bin/cat" or "/usr/ucb/more"
 * If defined, it can be overridden by the environment variable PAGER.
 * Hack will use its internal pager if DEF_PAGER is not defined.
 * (This might be preferable for security reasons.)
 * #define DEF_PAGER	".../mydir/mypager"
 */



/*
 * If you define MAIL, then the player will be notified of new mail
 * when it arrives.  If you also define DEF_MAILREADER then this will
 * be the default mail reader, and can be overridden by the environment
 * variable MAILREADER; otherwise an internal pager will be used.
 * A stat system call is done on the mailbox every MAILCKFREQ moves.
 */

/* Deliver mail during the game */
#undef MAIL

/* The Andrew Message System does mail a little differently from normal
 * UNIX.  Mail is deposited in the user's own directory in ~/Mailbox
 * (another directory).  MAILBOX is the element that will be added on to
 * the user's home directory path to generate the Mailbox path - just in
 * case other Andrew sites do it differently from CMU.
 *
 *		dan lovinger
 *		dl2n+@andrew.cmu.edu (dec 19 1989)
 */

/* use Andrew message system for mail */
#undef AMS

/* NO_MAILREADER is for kerberos authentcating filesystems where it is
 * essentially impossible to securely exec child processes, like mail
 * readers, when the game is running under a special token.
 *
 *             dan
 */

/* have mail daemon just tell player of mail */
/* #define NO_MAILREADER */

#ifdef	MAIL
# if defined(BSD) || defined(ULTRIX)
#  ifdef AMS
#define AMS_MAILBOX	"/Mailbox"
#  else
#define DEF_MAILREADER	"/usr/ucb/Mail"
#  endif
#else
# if defined(SYSV) || defined(DGUX) || defined(HPUX)
#  ifdef M_XENIX
#define DEF_MAILREADER	"/usr/bin/mail"
#  else
#   ifdef __sgi
#define DEF_MAILREADER	"/usr/sbin/Mail"
#   else
#define DEF_MAILREADER	"/usr/bin/mailx"
#   endif
#  endif
# else
#define DEF_MAILREADER	"/bin/mail"
# endif
#endif

#define MAILCKFREQ	50
#endif	/* MAIL */



#ifdef COMPRESS
/* Some implementations of compress need a 'quiet' option.
 * If you've got one of these versions, put -q here.
 * You can also include any other strange options your compress needs.
 * If you have a normal compress, just leave it commented out.
 */
/* #define COMPRESS_OPTIONS	"-q"	*/
#endif

#define FCMASK	0660	/* file creation mask */


/*
 * The remainder of the file should not need to be changed.
 */

/*
 * BSD/ULTRIX systems are normally the only ones that can suspend processes.
 * Suspending NetHack processes cleanly should be easy to add to other systems
 * that have SIGTSTP in the Berkeley sense.  Currently the only such systems
 * known to work are HPUX and AIX 3.1; other systems will probably require
 * tweaks to unixtty.c and ioctl.c.
 *
 * POSIX defines a slightly different type of job control, which should be
 * equivalent for NetHack's purposes.  POSIX_JOB_CONTROL should work on
 * various recent SYSV versions (with possibly tweaks to unixtty.c again).
 */
#ifndef POSIX_JOB_CONTROL
# if defined(BSD) || defined(ULTRIX) || defined(HPUX) || defined(AIX_31)
#  define BSD_JOB_CONTROL
# else
#  if defined(SVR4)
#   define POSIX_JOB_CONTROL
#  endif
# endif
#endif
#if defined(BSD_JOB_CONTROL) || defined(POSIX_JOB_CONTROL) || defined(AUX)
#define SUSPEND		/* let ^Z suspend the game */
#endif


#ifndef REDO
#define Getchar nhgetch
#endif
#define tgetch getchar

#define SHELL		/* do not delete the '!' command */

#ifdef STDC_HEADERS
#include <stdlib.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_SYS_WAIT_H
#include <sys/wait.h>
#endif

#if !defined(HAVE_MEMCPY) && defined(HAVE_BCOPY)
#define memcpy(d, s, n)		bcopy(s, d, n)
#endif

#if !defined(HAVE_MEMCMP) && defined(HAVE_BCMP)
#define memcmp(s1, s2, n)	bcmp(s2, s1, n)
#endif

#ifndef HAVE_INDEX
#define index	strchr
#endif
#ifndef HAVE_RINDEX
#define rindex	strrchr
#endif

#define E extern


#define SIG_RET_TYPE RETSIGTYPE (*)()

/* Define the filename size for savegame files */
#ifdef UNIX
#define SAVESIZE       (PL_NSIZ + 63)  /* save/99999player.e */
#else
# ifdef VMS
#define SAVESIZE       (PL_NSIZ + 22)  /* [.save]<uid>player.e;1 */
# else
#define SAVESIZE       FILENAME        /* from macconf.h or pcconf.h */
# endif
#endif

#include <sys/types.h>
#include "global.h"	/* Define everything else according to choices above */

#endif /* __NH_CONFIG_H__ */

