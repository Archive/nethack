/*
 * spkr.h -- interface definitions for speaker ioctl()
 *
 * v1.1 by Eric S. Raymond (esr@snark.thyrsus.com) Feb 1990
 *      modified for 386bsd by Andrew A. Chernov <ache@astral.msk.su>
 */

#ifndef _SPKR_H_
#define _SPKR_H_

#ifdef __386BSD__
#ifndef KERNEL
#include <sys/ioctl.h>
#else
#include "ioctl.h"
#endif

#define SPKRTONE        _IOW('S', 1, tone_t)    /* emit tone */
#define SPKRTUNE        _IO('S', 2)             /* emit tone sequence*/
#else /* SYSV */
#define	SPKRIOC		('S'<<8)
#define	SPKRTONE	(SPKRIOC|1)	/* emit tone */
#define	SPKRTUNE	(SPKRIOC|2)	/* emit tone sequence*/
#endif

typedef struct
{
    int	frequency;	/* in hertz */
    int duration;	/* in 1/100ths of a second */
}
tone_t;

#endif /* _SPKR_H_ */
/* spkr.h ends here */
