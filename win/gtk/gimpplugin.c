/*
 * Nethack tiletext format plugin for the GIMP
 *
 * This code is covered by the Nethack Public License
 *
 * Hacked by Rockwalrus (Nathan Summers)
 */

#include <string.h>
#include "tilelib.h"
#include "libgimp/gimp.h"

#define SIZE(x) (int)(sizeof(x)/sizeof(x[0]))

static void query (void);
static void   run        (char    *name,
                          int      nparams,
                          GParam  *param,
                          int     *nreturn_vals,
                          GParam **return_vals);

static gint32 load_image(char *filename);

GPlugInInfo PLUG_IN_INFO =
{
  NULL,    /* init_proc */
  NULL,    /* quit_proc */
  query,   /* query_proc */
  run,     /* run_proc */
};

MAIN ()


static void query(void)
{
	static GParamDef load_args[] = 
	{
		{PARAM_INT32, "run_mode", "Interactive, Non-interactive"},
		{PARAM_STRING, "filename", "Name of file to load"},
		{PARAM_STRING, "raw_filename", "Name of file to load"},
	};
	
	static GParamDef load_retvals[] =
	{
		{PARAM_IMAGE, "image", "Output image"},
	};
		
	static int nload_args = SIZE(load_args);
	static int nload_retvals = SIZE(load_retvals);
	
	gimp_install_procedure ("file_tiletext_load",
                          "loads NetHack TileText format files",
                          "This works just like any other file plugin. :)",
                          "Nathan Summers",
                          "Nathan Summers, Stichting Mathematisch Centrum, M. Stephenson",
                          "1998",
                          "<Load>/NetHack TileText",
			  NULL,
                          PROC_PLUG_IN,
                          nload_args, nload_retvals,
                          load_args, load_retvals);
			  
	gimp_register_magic_load_handler ("file_tiletext_load", "txt", "", "0,string,A = (");
}

static void run (char *name, int nparam, GParam *param, int *nretvals, GParam **retvals)
{
	static GParam values[2];
	gint32 image_ID;
	
	*nretvals=1;
	*retvals = values;
	
	values[0].type = PARAM_STATUS;
	values[0].data.d_status = STATUS_CALLING_ERROR;
	
	image_ID = load_image (param[1].data.d_string);
	
	if (image_ID != -1)
	{
		*nretvals=2;
		values[0].data.d_status = STATUS_SUCCESS;
		values[1].type = PARAM_IMAGE;
		values[1].data.d_image = image_ID;
	}
	else
	{
		values[0].data.d_status = STATUS_EXECUTION_ERROR;
	}
}

static gint32 load_image (char *filename)
{
	gint32 image_ID, layer_ID;
	tile tile;
	tiletext_file *f;
	GPixelRgn rgn;
	unsigned char *translation, *t;
	GDrawable *drawable;
	int x, y;
	f=tiletext_open(filename, "r");
	if (!f)
		return (-1);
		tiletext_read_colormap(f);
		tiletext_read_size(f);
	
	image_ID = gimp_image_new (f->xsize, f->ysize, RGB);
	translation = g_new(unsigned char, f->xsize * f->ysize * 3);
	gimp_image_set_filename(image_ID, filename);
	
	while (tiletext_read(f,&tile))
	{
		t=translation;
		for (y = 0; y<f->ysize; y++)
			for (x = 0; x< f->xsize; x++)
			{
				*t++=tile.data[y][x].r;
				*t++=tile.data[y][x].g;
				*t++=tile.data[y][x].b;
			}
		
		layer_ID = gimp_layer_new(image_ID, tile.name, f->xsize, f->ysize, RGB_IMAGE, 100, NORMAL_MODE);
		gimp_image_add_layer (image_ID, layer_ID, 0);
		
		drawable = gimp_drawable_get(layer_ID);
		gimp_pixel_rgn_init(&rgn, drawable, 0, 0, drawable->width, drawable->height, TRUE, FALSE);
		gimp_pixel_rgn_set_rect (&rgn, translation, 0, 0, f->xsize, f->ysize);
		gimp_drawable_flush(drawable);
		}
	
	return image_ID;
}
	
	

